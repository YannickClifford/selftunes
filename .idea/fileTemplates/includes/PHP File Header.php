/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - ${FILE_NAME}
 * Description  :   [Description]
 *
 * Created      :   ${DATE}
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with ${PRODUCT_NAME}.
 */