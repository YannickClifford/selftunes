<?php
/**
 * Author       :   Yannick.BAUDRAZ@cpnv.ch
 * Project      :   SelfTunes - config.php
 * Description  :   Config file with constants
 *
 * Created      :   06.07.2019
 * Updates      :   [dd.mm.yyyy author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

define('DIR_ALBUMS_IMAGES', 'views/assets/images/albums');
define('GENERIC_ALBUM_PATH', 'views/assets/images/albums/generic_cover_selftunes.jpg');
define('IS_AJAX_REQ', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
