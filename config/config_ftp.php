<?php
/**
 * Author       :   Yannick.BAUDRAZ@cpnv.ch
 * Project      :   SelfTunes - config_ftp.php
 * Description  :   Configuration file for FTP
 *
 * Created      :   14.07.2019
 * Updates      :   [dd.mm.yyyy author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

define('FTP_HOST', 'baudraz.internet-box.ch');
define('FTP_USERNAME', 'SelfTunes');
define('FTP_PASSWORD', 'SelfTunesForCPNV');
define('FTP_URL', 'ftp://' . FTP_USERNAME . ':' . FTP_PASSWORD . '@' . FTP_HOST);
