<?php
/**
 * Author       :   Yannick.BAUDRAZ@cpnv.ch
 * Project      :   SelfTunes - config_database.php
 * Description  :   Configuration file for the database.
 *
 * Created      :   12.07.2019
 * Updates      :   [dd.mm.yyyy author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

define('DB_SQL_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_PORT', 3306);
define('DB_CHARSET', 'utf8');
define('DB_NAME', 'selftunes');
define('DB_USER_NAME', 'selftunesAdmin');
define('DB_USER_PWD', 'SelfTunes-CPNV');
