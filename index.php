<?php
/**
 * Author       :   Yannick.BAUDRAZ@cpnv.ch
 * Project      :   SelfTunes - index.php
 * Description  :   [Description]
 *
 * Created      :   05.07.2019
 * Updates      :   09.07.2019 - Yannick.BAUDRAZ@cpnv.ch
 *                      Add Composer autoloader
 *
 * Git source   :   https://bitbucket.org/YannickClifford/selftunes/src/master/index.php
 *
 * Created with PhpStorm.
 */

//region Preparations
namespace SelfTunes\Controllers;

require_once 'config/config.php';
require_once 'vendor/autoload.php';

session_start();
createSession();

setlocale(LC_ALL, 'FR_CH.UTF8');
//endregion

//region Router
if (isset($_GET['AJAX'])) { // AJAX requests which are NOT asked for a page.
    $ajaxRequest = $_GET['AJAX'];
    switch ($ajaxRequest) {
        case 'get-song-json':
            songJsonRequest($_POST['songId'] ?? 0);
            break;
        case 'update-plays':
            updatePlaysRequest($_POST['songId']);
            break;
        case 'create-playlist':
            newPlaylist($_POST['playlistName'], $_SESSION['user-id']);
            break;
        case 'delete-playlist':
            deletePlaylistRequest($_POST['playlistId']);
            break;
        case 'add-to-playlist':
            addToPlaylistRequest($_POST['playlistId'], $_POST['songId']);
            break;
        case 'remove-from-playlist':
            removeFromPlaylistRequest($_POST['playlistId'], $_POST['songId']);
            break;
        case 'update-account':
            updateAccount($_POST);
            break;
    }
} else {
    $action = (isset($_GET{'action'})) ? $_GET['action'] : null;
    if (!empty($_SESSION['user-id'])) { // AJAX requests which are asked for a page.
        switch ($action) {
            case 'home':
            default:
                home();
                break;
            case 'album':
                albumPage($_GET['id'] ?? 0, $_SESSION['user-id']);
                break;
            case 'artist':
                artistPage($_GET['id'] ?? 0, $_SESSION['user-id']);
                break;
            case 'logout':
                logout();
                break;
            case 'search':
                search($_GET['search-term'] ?? '', $_SESSION['user-id']);
                break;
            case 'playlists':
                playlistsPage($_SESSION['user-id']);
                break;
            case 'playlist':
                singlePlaylistPage($_GET['id'], $_SESSION['user-id']);
                break;
            case 'account':
                accountPage($_SESSION['user-id']);
                break;
        }
    } else {
        switch ($action) {
            case 'login':
            default:
                login($_POST);
                break;
            case 'register':
                register($_POST);
                break;
        }
    }
}
//endregion
