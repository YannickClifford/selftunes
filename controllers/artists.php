<?php /** @noinspection PhpUnusedLocalVariableInspection */

/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - artists.php
 * Description  :   [Description]
 *
 * Created      :   30.07.2019
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

namespace SelfTunes\Controllers;

use SelfTunes\Models\Database\ArtistsManager;
use SelfTunes\Models\Database\ManagerFactory;

/**
 * Redirect to the artist page.
 *
 * @param int $artistId
 * @param int $userId
 */
function artistPage(int $artistId, int $userId)
{
    /** @var ArtistsManager $artistManager */
    $artist = ($artistManager = ManagerFactory::createManager('artists'))->selectOne($artistId);
    $artistAlbums = $artistManager->selectArtistAlbums($artistId);
    $artistSongs = $artistManager->selectArtistSongs($artistId, true);
    $artistAllSongs = $artistManager->selectArtistSongs($artistId);
    $userPlaylists = ManagerFactory::createManager('users')->selectUserPlaylists($userId);

    require 'views/pages/artist.php';
}
