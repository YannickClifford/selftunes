<?php
/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - controller.php
 * Description  :   [Description]
 *
 * Created      :   29.07.2019
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

namespace SelfTunes\Controllers;

/**
 * Redirect to the page wanted.
 *
 * @param string $page  Name of the page. Need to be in the folder 'views/pages/'
 * @param bool   $error Say if there was an error.
 */
function redirect(string $page, bool $error = false)
{
    $_GET[$page . 'Error'] = $error;
    $_GET['action'] = $page;
    /** @noinspection PhpIncludeInspection */
    require 'views/pages/' . $page . '.php';
}
