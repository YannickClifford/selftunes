<?php /** @noinspection PhpUnusedLocalVariableInspection */

/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - users.php
 * Description  :   [Description]
 *
 * Created      :   28.07.2019
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

namespace SelfTunes\Controllers;

use SelfTunes\Models\Database\Entity\Users;
use SelfTunes\Models\Database\ManagerFactory;
use SelfTunes\Models\Database\UsersManager;

/**
 * Manage login request.
 *
 * @param array $loginRequest Contain login fields required to authenticate the user.($_POST variable)
 */
function login($loginRequest): void
{
    if (isset($loginRequest['email']) && isset($loginRequest['password'])) {
        //extract login parameters
        $userEmailAddress = $loginRequest['email'];
        $userPsw = $loginRequest['password'];

        /** @var UsersManager $userManager */
        $userManager = ManagerFactory::createManager('users');
        if ($userManager->checkLogin($userEmailAddress, $userPsw)) {
            $userId = $userManager->selectUserId($userEmailAddress);
            createSession($userId, $userEmailAddress);
            home();
        } else {
            redirect('login', true);
        }
    } else {
        redirect('login');
    }
}

/**
 * Manage a register request.
 *
 * @param array $registerRequest Contain register fields required to register the user.($_POST variable)
 */
function register($registerRequest): void
{
    if (isset($registerRequest['email'])
        && isset($registerRequest['password'])
        && isset($registerRequest['repeat-password'])
        && isset($registerRequest['check'])) {

        //region Extract register form inputs
        $userInfos['userEmail'] = $registerRequest['email'];
        $userInfos['userPsw'] = $registerRequest['password'];
        $userInfos['userFName'] = !empty($registerRequest['fname']) ? $registerRequest['fname'] : null;
        $userInfos['userLName'] = !empty($registerRequest['lname']) ? $registerRequest['lname'] : null;

        $userPswRepeat = $registerRequest['repeat-password'];
        //endregion

        if ($userInfos['userPsw'] == $userPswRepeat) {
            /** @var UsersManager $userManager */
            $userManager = ManagerFactory::createManager('users');
            if ($userManager->registerNewAccount($userInfos)) {
                $userId = $userManager->selectUserId($userInfos['userEmail']);
                createSession($userId, $userInfos['userEmail']);
                home();
            }
        } else {
            redirect('register', true);
        }
    } else {
        redirect('register');
    }
}

/**
 * Create a new user session
 *
 * @param string $userId
 * @param string $userEmailAddress : user unique id
 */
function createSession(string $userId = null, string $userEmailAddress = null): void
{
    if ($userId)
        $_SESSION['user-id'] = $userId;

    if ($userEmailAddress)
        $_SESSION['user-email'] = $userEmailAddress;

    //Push a random playlist if the page load isn't from AJAX and create little random playlist for it.
    if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
        $songs = (ManagerFactory::createManager('songs'))->selectForStartApp();
        $songsJSON = json_encode($songs);
        $_SESSION['start-trackList'] = $songsJSON;
    }
}

/**
 * Manage logout request
 */
function logout(): void
{
    $_SESSION = [];
    session_destroy();
    login([]);
}

/**
 * Open the user's account page.
 *
 * @param int $userId
 */
function accountPage(int $userId)
{
    /** @var Users $user */
    $user = ManagerFactory::createManager('users')->selectOne($userId);

    require 'views/pages/account.php';
}

/**
 * Update user's account info
 *
 * @param array $updateRequest Input data from the form.
 */
function updateAccount(array $updateRequest)
{
    $error = false;
    if (
        // Email is empty
        empty($updateRequest['email'])
        // Email is not correctly confirmed
        || ($updateRequest['email'] !== $updateRequest['repeat-email'])
        // Password is not empty and not correctly confirmed
        || (!empty($updateRequest['password']) && $updateRequest['password'] !== $updateRequest['repeat-password'])
    ) {
        // No need to update
        $error = true;
    }

    if (!$error) {
        $userInfo = [
            'id' => $_SESSION['user-id'],
            'email' => $updateRequest['email'],
            'fname' => $updateRequest['fname'],
            'lname' => $updateRequest['lname'],
            'pwd' => $updateRequest['password']
        ];

        ManagerFactory::createManager('users')->updateUser($userInfo);
    }

    // Send the value of error to Javascript.
    echo json_encode($error);
}
