<?php /** @noinspection PhpUnusedLocalVariableInspection */

/**
 * Author       :   Yannick.BAUDRAZ@cpnv.ch
 * Project      :   SelfTunes - albums.php
 * Description  :   Controller for albums
 *
 * Created      :   17.07.2019
 * Updates      :   22.07.2019
 *                      Add home controller
 *                  23.07.2019
 *                      Rename file to controller.php in albums.php
 *
 * Git source   :   https://bitbucket.org/YannickClifford/selftunes/src/master/controllers/albums.php
 *
 * Created with PhpStorm.
 */

namespace SelfTunes\Controllers;

use SelfTunes\Models\Database\ManagerFactory;

/**
 * Redirect the user to the home page.
 */
function home()
{
    $_GET['action'] = $action = "home";
    $albums = (ManagerFactory::createManager('albums'))->selectForHome();

    require "views/pages/home.php";
}

/**
 * Redirect to the album page.
 *
 * @param int $albumId
 * @param int $userId
 */
function albumPage(int $albumId, int $userId)
{
    $album = ($albumManager = ManagerFactory::createManager('albums'))->selectOne($albumId);
    $albumManager->selectSongsNumber($albumId, $album);
    $albumManager->selectAlbumGenre($albumId, $album);
    $albumTracks = $albumManager->selectAlbumSongs($albumId);
    $userPlaylists = ManagerFactory::createManager('users')->selectUserPlaylists($userId);

    require 'views/pages/album.php';
}
