<?php
/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - songs.php
 * Description  :   [Description]
 *
 * Created      :   25.07.2019
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

namespace SelfTunes\Controllers;

use SelfTunes\Models\Database\ManagerFactory;

/**
 * Get the songs needed in the music player
 * AJAX process.
 *
 * @param int $songId
 */
function songJsonRequest(int $songId)
{
    if (!empty($songId)) {
        $songManager = ManagerFactory::createManager('songs');
        $song = $songManager->selectOneForAJAX($songId);
        echo json_encode($song, JSON_FORCE_OBJECT);
    }
}

/**
 * Update plays by one if the song is played.
 * AJAX process.
 *
 * @param int $songId
 */
function updatePlaysRequest(int $songId)
{
    if (!empty($songId)) {
        $songManager = ManagerFactory::createManager('songs');
        $songManager->updatePlays($songId);
    }
}
