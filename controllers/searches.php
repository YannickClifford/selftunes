<?php /** @noinspection PhpUnusedLocalVariableInspection */

/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - searches.php
 * Description  :   [Description]
 *
 * Created      :   06.08.2019
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

namespace SelfTunes\Controllers;

use SelfTunes\Models\Database\ManagerFactory;

/**
 * Open the search page.
 * If there is research terms, take data.
 *
 * @param string $searchTerm
 * @param int    $userId
 */
function search(string $searchTerm, int $userId)
{
    $songs = $artists = $albums = [];
    if (!empty($searchTerm)) {
        $searchTerm = urldecode($searchTerm);
        $songs = (ManagerFactory::createManager('songs')->searchSongs($searchTerm));
        $artists = (ManagerFactory::createManager('artists')->searchArtists($searchTerm));
        $albums = (ManagerFactory::createManager('albums')->searchAlbums($searchTerm));
        $userPlaylists = ManagerFactory::createManager('users')->selectUserPlaylists($userId);
    }

    require 'views/pages/search.php';
}
