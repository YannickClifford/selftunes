<?php /** @noinspection PhpUnusedLocalVariableInspection */

/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - playlists.php
 * Description  :   [Description]
 *
 * Created      :   07.08.2019
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

namespace SelfTunes\Controllers;

use SelfTunes\Models\Database\ManagerFactory;
use SelfTunes\Models\Database\PlaylistsManager;

/**
 * Open the page of playlists.
 *
 * @param int $userId
 */
function playlistsPage(int $userId): void
{
    $userPlaylists = ManagerFactory::createManager('users')->selectUserPlaylists($userId);

    require 'views/pages/playlists.php';
}

/**
 * Open a single playlist page.
 *
 * @param int $playlistId
 * @param int $userId
 */
function singlePlaylistPage(int $playlistId, int $userId)
{
    /** @var PlaylistsManager $playlistManager */
    $playlist = ($playlistManager = ManagerFactory::createManager('playlists'))->selectOne($playlistId);
    $playlistTracks = $playlistManager->selectPlaylistSongs($playlistId);
    $userPlaylists = ManagerFactory::createManager('users')->selectUserPlaylists($userId);

    require 'views/pages/playlist.php';
}

/**
 * Process for a new playlist.
 *
 * @param string $playlistName
 * @param string $ownerId
 */
function newPlaylist(string $playlistName, string $ownerId): void
{
    ManagerFactory::createManager('playlists')->createPlaylist($playlistName, $ownerId);
}

/**
 * Process for deleting a playlist.
 *
 * @param int $playlistId
 */
function deletePlaylistRequest(int $playlistId)
{
    ManagerFactory::createManager('playlists')->deleteOne($playlistId);
}

/**
 * Process for adding a song to a playlist.
 *
 * @param int $playlistId
 * @param int $songId
 */
function addToPlaylistRequest(int $playlistId, int $songId)
{
    ManagerFactory::createManager('playlists')->insertSongInPlaylist($playlistId, $songId);
}

function removeFromPlaylistRequest(int $playlistId, int $songId)
{
    ManagerFactory::createManager('playlists')->deleteSongFromPlaylist($playlistId, $songId);
}
