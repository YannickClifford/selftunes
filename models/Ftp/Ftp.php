<?php
/**
 * Author       :   Yannick.BAUDRAZ@cpnv.ch
 * Project      :   SelfTunes - Ftp.php
 * Description  :   Ftp class
 *
 * Created      :   11.07.2019
 * Updates      :   23.07.2019 - Yannick Baudraz
 *                      Add method to download file
 *
 * Git source   :   https://bitbucket.org/YannickClifford/selftunes/src/master/models/Ftp/Ftp.php
 *
 * Created with PhpStorm.
 */

namespace SelfTunes\Models\Ftp;

require_once dirname(dirname(__DIR__)) . '/config/config_ftp.php';

class Ftp
{

    /**
     * @var resource
     */
    private $connection;

    /**
     * @var string|
     */
    private $ftpServer;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * Ftp constructor.
     *
     * @param string $ftpServer The Url to the ftp server.
     *                          Shouldn't have any trailing slashes and shouldn't be prefixed with ftp://.
     * @param string $username
     * @param string $password
     */
    public function __construct(string $ftpServer = null, string $username = null, string $password = null)
    {
        $this->ftpServer = $ftpServer ? $ftpServer : FTP_HOST;
        $this->username = $username ? $username : FTP_USERNAME;
        $this->password = $password ? $password : FTP_PASSWORD;

        $this->connect();
        $this->login();
        $this->setPassive();
    }

    /**
     * Ftp constructor.
     * Close the connection.
     */
    public function __destruct() { $this->close(); }

    /**
     * Connects to the FTP server.
     *
     * @param bool       $ssl     Says if the connection is in ssl or not.
     * @param int|string $port    Specifies an alternate port to connect to.
     * @param int|string $timeout Specifies the timeout for all subsequent network operations.
     *
     * @return resource The connection resource
     */
    public function connect(bool $ssl = false, $port = 21, $timeout = 90)
    {
        $connectionTmp = $ssl
            ? ftp_ssl_connect($this->ftpServer, $port, $timeout)
            : ftp_connect($this->ftpServer, $port, $timeout);

        if ($this->connection) {
            $this->close();
        }

        $this->connection = $connectionTmp;

        return $this->connection;
    }

    /**
     * Close the connection.
     *
     * @return bool|null
     */
    public function close()
    {
        $result = null;

        if ($this->connection) {
            $result = ftp_close($this->connection);
            $this->connection = null;
        }

        return $result;
    }

    /**
     * Logs to the FTP server.
     *
     * @return bool
     */
    public function login() { return ftp_login($this->connection, $this->username, $this->password); }

    /**
     * Turns passive mode on or off.
     *
     * @param bool $passivity
     *
     * @return bool
     */
    public function setPassive(bool $passivity = true) { return ftp_pasv($this->connection, $passivity); }

    /**
     * Get all files, recursively, of a directory in the FTP server.
     *
     * @param string $path
     *
     * @return array
     */
    public function recurseFileList(string $path)
    {
        static $allFiles = [];

        $contents = ftp_nlist($this->connection, $path);
        foreach ($contents as $currentFile) {
            if (strpos($currentFile, '.') === false)
                self::recurseFileList($currentFile);

            $allFiles[$path][] = substr($currentFile, strlen($path) + 1);
        }

        return $allFiles;
    }

    /**
     * Download a file from the FTP server.
     *
     * @param string $path
     * @param string $localPath
     * @param int    $mode The transfer mode. Must be either FTP_ASCII(1) or FTP_BINARY(2).
     *
     * @return bool
     */
    public function downloadFile(string $path, string $localPath = null, int $mode = FTP_ASCII): bool
    {
        if (!$localPath)
            $localPath = __DIR__ . '/' . basename($path);

        return ftp_get($this->connection, $localPath, '/' . $path, $mode);
    }
}
