<?php
/**
 * Author       :   Yannick.BAUDRAZ@cpnv.ch
 * Project      :   SelfTunes - ManagerFactory.php
 * Description  :   Manager factory
 *
 * Created      :   21.07.2019
 * Updates      :   [dd.mm.yyyy author]
 *                      [description of update]
 *
 * Git source   :   https://bitbucket.org/YannickClifford/selftunes/src/master/models/Database/ManagerFactory.php
 *
 * Created with PhpStorm.
 */

namespace SelfTunes\Models\Database;

/**
 * Class ManagerFactory
 *
 * @package SelfTunes\Models\Database
 */
class ManagerFactory
{

    /**
     * Create a Manager class, from the name of his entity.
     *
     * @param string $entity
     *
     * @return DatabaseManager|AlbumsManager|SongsManager|UsersManager|ArtistsManager|PlaylistsManager
     */
    public static function createManager(string $entity)
    {
        $className = __NAMESPACE__ . '\\' . ucfirst($entity) . 'Manager';

        return new $className();
    }
}
