<?php
/**
 * Author       :   Yannick.BAUDRAZ@cpnv.ch
 * Project      :   SelfTunes - EntityFactory.php
 * Description  :   Entity Factory
 *
 * Created      :   20.07.2019
 * Updates      :   22.07.2019 - Yannick Baudraz
 *                      Update switch to only home case
 *
 * Git source   :   https://bitbucket.org/YannickClifford/selftunes/src/master/models/Entity/EntityFactory.php
 *
 * Created with PhpStorm.
 */

namespace SelfTunes\Models\Database\Entity;

/**
 * Class EntityFactory
 *
 * @package SelfTunes\Models\Database\Entity
 */
class EntityFactory
{

    /**
     * Create a child of an Entity class, from his name.
     *
     * @param string $entity
     * @param array  $data
     *
     * @return Albums|Songs|Artists|Playlists|Users
     */
    public static function createEntity(string $entity, array $data)
    {
        $entityClass = __NAMESPACE__ . '\\' . ucfirst($entity);

        return new $entityClass($data);
    }
}
