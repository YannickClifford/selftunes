<?php
/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - Users.php
 * Description  :   [Description]
 *
 * Created      :   12.08.2019
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

namespace SelfTunes\Models\Database\Entity;


/**
 * Class Users
 *
 * @package SelfTunes\Models\Database\Entity
 */
class Users extends Entity
{

    /**
     * @var int
     */
    private $id_user;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $firstname = null;

    /**
     * @var string
     */
    private $lastname = null;

    /**
     * Hashed password.
     *
     * @var string
     */
    private $password;

    /**
     * Path to the profile picture.
     *
     * @var string
     */
    private $profile_picture = null;

    /**
     * @var string
     */
    private $date_registration;

    /**
     * Getter of $id_user.
     *
     * @return int
     */
    public function getIdUser(): int { return $this->id_user; }

    /**
     * Setter of $id_user.
     *
     * @param int $id_user
     */
    public function setIdUser(int $id_user): void { $this->id_user = $id_user; }

    /**
     * Getter of $email.
     *
     * @return string
     */
    public function getEmail(): string { return $this->email; }

    /**
     * Setter of $email.
     *
     * @param string $email
     */
    public function setEmail(string $email): void { $this->email = $email; }

    /**
     * Getter of $firstname.
     *
     * @return string|null
     */
    public function getFirstname() { return $this->firstname; }

    /**
     * Setter of $firstname.
     *
     * @param string $firstname
     */
    public function setFirstname(string $firstname = null): void { $this->firstname = $firstname; }

    /**
     * Getter of $lastname.
     *
     * @return string|null
     */
    public function getLastname() { return $this->lastname; }

    /**
     * Setter of $lastname.
     *
     * @param string $lastname
     */
    public function setLastname(string $lastname = null): void { $this->lastname = $lastname; }

    /**
     * Getter of $password.
     *
     * @return string
     */
    public function getPassword(): string { return $this->password; }

    /**
     * Setter of $password.
     *
     * @param string $password
     */
    public function setPassword(string $password): void { $this->password = $password; }

    /**
     * Getter of $profile_picture.
     *
     * @return string|null
     */
    public function getProfilePicture() { return $this->profile_picture; }

    /**
     * Setter of $profile_picture.
     *
     * @param string $profile_picture
     */
    public function setProfilePicture(string $profile_picture = null): void { $this->profile_picture = $profile_picture; }

    /**
     * Getter of $date_registration.
     *
     * @return string
     */
    public function getDateRegistration(): string { return $this->date_registration; }

    /**
     * Setter of $date_registration.
     *
     * @param string $date_registration
     */
    public function setDateRegistration(string $date_registration): void { $this->date_registration = $date_registration; }
}
