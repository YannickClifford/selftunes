<?php
/**
 * Author       :   Yannick.BAUDRAZ@cpnv.ch
 * Project      :   SelfTunes - Entity.php
 * Description  :   Entity abstract class
 *
 * Created      :   21.07.2019
 * Updates      :   22.07.2019 -  Yannick Baudraz
 *                      Debug constructor : Returned always null => add "!" at the beginning
 *
 * Git source   :   https://bitbucket.org/YannickClifford/selftunes/src/master/models/Entity/Entity.php
 *
 * Created with PhpStorm.
 */

namespace SelfTunes\Models\Database\Entity;

/**
 * Class abstract Entity
 *
 * @package SelfTunes\Models\Database\Entity
 */
abstract class Entity
{

    /**
     * ManagerChild constructor.
     *
     * @param array $data Contains the data of the entity and set them in their properties.
     */
    public function __construct(array $data = null) { !$data ?: $this->hydrate($data); }

    /**
     * Set all the data from the array if the have their own setter.
     *
     * @param array $data
     *
     * @example hydrate($data["dbManager" => $dbConnection]) -> setDbManager($dbConnection) is called.
     */
    public function hydrate(array $data)
    {
        foreach ($data as $key => $value) {
            if (strpos($key, '_')) {
                $keyExploded = explode('_', $key);
                $keyExploded[] = ucfirst(array_pop($keyExploded));
                $key = implode($keyExploded);
            }

            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method))
                $this->$method($value);
            else
                $this->$key = $value;
        }
    }
}
