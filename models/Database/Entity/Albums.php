<?php
/**
 * Author       :   Yannick.BAUDRAZ@cpnv.ch
 * Project      :   SelfTunes - Albums.php
 * Description  :   Albums representative class
 *
 * Created      :   21.07.2019
 * Updates      :   [dd.mm.yyyy author]
 *                      [description of update]
 *
 * Git source   :   https://bitbucket.org/YannickClifford/selftunes/src/master/models/Entity/Albums.php
 *
 * Created with PhpStorm.
 */

namespace SelfTunes\Models\Database\Entity;

/**
 * Class Albums
 *
 * @package SelfTunes\Models\Database\Entity
 */
class Albums extends Entity
{

    /**
     * @var int
     */
    private $id_album = 0;

    /**
     * @var int
     */
    private $artist_id = 0;

    /**
     * @var string
     */
    private $name = '';

    /**
     * Year in format "Y"
     *
     * @var int
     */
    private $year = 0;

    /**
     * File name the image
     *
     * @var string
     */
    private $image = '';

    /**
     * Getter of $id_album.
     *
     * @return int
     */
    public function getIdAlbum(): int { return $this->id_album; }

    /**
     * Setter of $id_album.
     *
     * @param int $id_album
     */
    public function setIdAlbum(int $id_album): void { $this->id_album = $id_album; }

    /**
     * Getter of $artist_id.
     *
     * @return int
     */
    public function getArtistId(): int { return $this->artist_id; }

    /**
     * Setter of $artist_id.
     *
     * @param int $artist_id
     */
    public function setArtistId(int $artist_id): void { $this->artist_id = $artist_id; }

    /**
     * Getter of $name.
     *
     * @return string
     */
    public function getName(): string { return $this->name; }

    /**
     * Setter of $name.
     *
     * @param string $name
     */
    public function setName(string $name): void { $this->name = $name; }

    /**
     * Getter of $year
     *
     * @return int
     */
    public function getYear(): int { return $this->year; }

    /**
     * Setter of $year
     *
     * @param int $year
     */
    public function setYear(int $year): void { $this->year = $year; }

    /**
     * Getter of $image.
     *
     * @return string
     */
    public function getImage(): string { return $this->image; }

    /**
     * Setter of $image.
     *
     * @param string $image
     */
    public function setImage(string $image): void
    {
        $this->image = $image ? DIR_ALBUMS_IMAGES . '/' . $image : GENERIC_ALBUM_PATH;
    }
}
