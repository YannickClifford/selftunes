<?php
/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - Songs.php
 * Description  :   [Description]
 *
 * Created      :   24.07.2019
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

namespace SelfTunes\Models\Database\Entity;

use DateTime;
use Exception;
use JsonSerializable;

/**
 * Class Songs
 *
 * @package SelfTunes\Models\Database\Entity
 */
class Songs extends Entity implements JsonSerializable
{

    /**
     * @var int
     */
    private $id_song;

    /**
     * @var int
     */
    private $genre_id;

    /**
     * @var int
     */
    private $album_id;

    /**
     * @var string
     */
    private $title;

    /**
     * Must be formattable for a time.
     *
     * @var string
     */
    private $duration;

    /**
     * @var string
     */
    private $path;

    /**
     * Track number related to the album.
     *
     * @var int
     */
    private $track_number;

    /**
     * Number of time the song was played.
     *
     * @var int
     */
    private $plays;

    /**
     * Specify data which should be serialized to JSON
     *
     * @link  https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize() { return (object)get_object_vars($this); }

    /**
     * Getter of $id_song.
     *
     * @return int
     */
    public function getIdSong(): int { return $this->id_song; }

    /**
     * Getter of $id_song.
     *
     * @param int $id_song
     */
    public function setIdSong(int $id_song): void { $this->id_song = $id_song; }

    /**
     * Getter of $genre_id.
     *
     * @return int
     */
    public function getGenreId(): int { return $this->genre_id; }

    /**
     * Getter of $genre_id.
     *
     * @param int $genre_id
     */
    public function setGenreId(int $genre_id): void { $this->genre_id = $genre_id; }

    /**
     * Getter of $album_id.
     *
     * @return int
     */
    public function getAlbumId(): int { return $this->album_id; }

    /**
     * Getter of $album_id.
     *
     * @param int $album_id
     */
    public function setAlbumId(int $album_id): void { $this->album_id = $album_id; }

    /**
     * Getter of $title.
     *
     * @return string
     */
    public function getTitle(): string { return $this->title; }

    /**
     * Getter of $title.
     *
     * @param string $title
     */
    public function setTitle(string $title): void { $this->title = $title; }

    /**
     * Getter of $duration.
     *
     * @return string
     */
    public function getDuration(): string
    {
        try {
            return $this->duration = (new DateTime($this->duration))->format('i:s');
        } catch (Exception $exception) {
            die($exception->getMessage());
        }
    }

    /**
     * Getter of $duration.
     *
     * @param string $duration
     */
    public function setDuration(string $duration): void { $this->duration = $duration; }

    /**
     * Getter of $path.
     *
     * @return string
     */
    public function getPath(): string { return $this->path; }

    /**
     * Getter of $path.
     *
     * @param string $path
     */
    public function setPath(string $path): void { $this->path = $path; }

    /**
     * Getter of $track_number.
     *
     * @return int
     */
    public function getTrackNumber(): int { return $this->track_number; }

    /**
     * Getter of $track_number.
     *
     * @param int $track_number
     */
    public function setTrackNumber(int $track_number): void { $this->track_number = $track_number; }

    /**
     * Getter of $plays.
     *
     * @return int
     */
    public function getPlays(): int { return $this->plays; }

    /**
     * Getter of $plays.
     *
     * @param int $plays
     */
    public function setPlays(int $plays): void { $this->plays = $plays; }
}
