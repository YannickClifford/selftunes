<?php
/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - Playlists.php
 * Description  :   [Description]
 *
 * Created      :   07.08.2019
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

namespace SelfTunes\Models\Database\Entity;


/**
 * Class Playlists
 *
 * @package SelfTunes\Models\Database\Entity
 */
class Playlists extends Entity
{

    /**
     * @var int
     */
    private $id_playlist = 0;

    /**
     * @var int
     */
    private $user_id = 0;

    /**
     * @var string
     */
    private $name = '';

    /**
     * Creation date of the playlist.
     * Format is : Y-m-d H:i:s
     *
     * @var string
     */
    private $date_creation = '';

    /**
     * Getter of $id_playlist.
     *
     * @return int
     */
    public function getIdPlaylist(): int { return $this->id_playlist; }

    /**
     * Setter of $id_playlist.
     *
     * @param int $id_playlist
     */
    public function setIdPlaylist(int $id_playlist): void { $this->id_playlist = $id_playlist; }

    /**
     * Getter of $user_id.
     *
     * @return int
     */
    public function getUserId(): int { return $this->user_id; }

    /**
     * Setter of $user_id.
     *
     * @param int $user_id
     */
    public function setUserId(int $user_id): void { $this->user_id = $user_id; }

    /**
     * Getter of $name.
     *
     * @return string
     */
    public function getName(): string { return $this->name; }

    /**
     * Setter of $name.
     *
     * @param string $name
     */
    public function setName(string $name): void { $this->name = $name; }

    /**
     * Getter of $date_creation.
     *
     * @return string
     */
    public function getDateCreation(): string { return $this->date_creation; }

    /**
     * Setter of $date_creation.
     *
     * @param string $date_creation
     */
    public function setDateCreation(string $date_creation): void { $this->date_creation = $date_creation; }
}
