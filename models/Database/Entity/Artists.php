<?php
/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - Artists.php
 * Description  :   [Description]
 *
 * Created      :   30.07.2019
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

namespace SelfTunes\Models\Database\Entity;


class Artists extends Entity
{

    /**
     * @var int
     */
    private $id_artist = 0;

    /**
     * @var string
     */
    private $name = '';

    /**
     * Getter of $id_artist.
     *
     * @return int
     */
    public function getIdArtist(): int { return $this->id_artist; }

    /**
     * Setter of $id_artist.
     *
     * @param int $id_artist
     */
    public function setIdArtist(int $id_artist): void { $this->id_artist = $id_artist; }

    /**
     * Getter of $name.
     *
     * @return string
     */
    public function getName(): string { return $this->name; }

    /**
     * Setter of $name.
     *
     * @param string $name
     */
    public function setName(string $name): void { $this->name = $name; }
}
