<?php
/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - PlaylistsManager.php
 * Description  :   [Description]
 *
 * Created      :   07.08.2019
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

namespace SelfTunes\Models\Database;

use SelfTunes\Models\Database\Entity\EntityFactory;
use SelfTunes\Models\Database\Entity\Playlists;
use SelfTunes\Models\Database\Entity\Songs;


class PlaylistsManager extends DatabaseManager
{

    /**
     * Create a new playlist in the database.
     *
     * @param string $playlistName
     * @param int    $ownerId
     *
     * @return bool
     */
    public function createPlaylist(string $playlistName, int $ownerId): bool
    {
        return $this->executeQuery(
            'INSERT INTO playlists (user_id, name) VALUES (:id, :name)',
            [':id' => $ownerId, ':name' => $playlistName]
        );
    }

    /**
     * Select one playlist.
     *
     * @param int $id
     *
     * @return Playlists
     */
    public function selectOne(int $id): Playlists
    {
        $this->executeQuery(
            '
                SELECT pl.id_playlist, pl.name, pl.date_creation, COUNT(sp.song_id) AS tracks_number
                FROM playlists AS pl
                    LEFT JOIN songs_playlist sp ON pl.id_playlist = sp.playlist_id
                WHERE pl.id_playlist = ?
            ',
            [$id]
        );

        return EntityFactory::createEntity('playlists', $this->fetchOne());
    }

    /**
     * Select all songs from a playlist.
     *
     * @param int $playlistId
     *
     * @return Songs[]
     */
    public function selectPlaylistSongs(int $playlistId): array
    {
        $this->executeQuery(
            '
                SELECT s.id_song, s.title, s.duration, s.track_number, ar.id_artist, ar.name AS artist_name
                FROM songs as s
                    INNER JOIN songs_playlist sp on s.id_song = sp.song_id
                    INNER JOIN albums al ON s.album_id = al.id_album
                    INNER JOIN artists ar ON al.artist_id = ar.id_artist
                WHERE sp.playlist_id = ?
                ORDER BY s.plays DESC
            ',
            [$playlistId]
        );

        $playlists = [];
        foreach ($this->fetchRecords() as $record)
            $playlists[] = EntityFactory::createEntity('songs', $record);

        return $playlists;
    }

    /**
     * Delete a playlist.
     *
     * @param int $id
     *
     * @return bool
     */
    public function deleteOne(int $id): bool
    {
        return $this->executeQuery('DELETE FROM playlists WHERE id_playlist = ?', [$id]);
    }

    /**
     * Add a song to a playlist.
     *
     * @param int $playlistId
     * @param int $songId
     *
     * @return bool
     */
    public function insertSongInPlaylist(int $playlistId, int $songId): bool
    {
        return $this->executeQuery(
            'INSERT INTO songs_playlist VALUES (:playlistId, :songId)',
            [':playlistId' => $playlistId, ':songId' => $songId]
        );
    }

    /**
     * Delete a song in a playlist.
     *
     * @param int $playlistId
     * @param int $songId
     *
     * @return bool
     */
    public function deleteSongFromPlaylist(int $playlistId, int $songId): bool
    {
        return $this->executeQuery(
            'DELETE FROM songs_playlist WHERE playlist_id = :playlistId AND song_id = :songId',
            [':playlistId' => $playlistId, ':songId' => $songId]
        );
    }
}
