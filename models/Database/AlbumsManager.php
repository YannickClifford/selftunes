<?php
/**
 * Author       :   Yannick.BAUDRAZ@cpnv.ch
 * Project      :   SelfTunes - AlbumsManager.php
 * Description  :   Class AlbumsManager inherited from DatabaseManager
 *
 * Created      :   20.07.2019
 * Updates      :   22.07.2019 - Yannick Baudraz
 *                      Add method for the home page
 *
 * Git source   :   https://bitbucket.org/YannickClifford/selftunes/src/master/models/Database/AlbumsManager.php
 *
 * Created with PhpStorm.
 */

namespace SelfTunes\Models\Database;

use SelfTunes\Models\Database\Entity\Albums;
use SelfTunes\Models\Database\Entity\EntityFactory;
use SelfTunes\Models\Database\Entity\Songs;

/**
 * Class AlbumsManager
 *
 * @package SelfTunes\Models\Database
 * @uses    ManagerChild
 */
class AlbumsManager extends DatabaseManager
{

    public const TABLE_NAME = 'Albums';

    /**
     * Get albums for the home page
     *
     * @return Albums[]
     */
    public function selectForHome(): array
    {
        $this->executeQuery('
            SELECT albums.id_album, albums.artist_id, albums.name, albums.image, artists.name as artist_name
            FROM albums
                INNER JOIN artists on albums.artist_id = id_artist
            ORDER BY RAND()'
        );

        $albums = [];
        foreach ($this->fetchRecords() as $record)
            $albums[] = EntityFactory::createEntity(self::TABLE_NAME, $record);

        return $albums;
    }

    /**
     * Select one album
     *
     * @param int $id
     *
     * @return Albums
     */
    public function selectOne(int $id): Albums
    {
        $this->executeQuery(
            '
                SELECT albums.id_album, 
                       albums.artist_id, 
                       albums.name, 
                       albums.year, 
                       albums.image, 
                       artists.name as artist_name
                FROM albums
                    INNER JOIN artists on albums.artist_id = id_artist
                WHERE id_album = :id
            ',
            [':id' => $id]
        );

        $record = $this->fetchOne();

        return EntityFactory::createEntity(self::TABLE_NAME, $record);
    }

    /**
     * Return the number of songs of the album.
     * If an album is passed as parameter, the track number is assigned directly to the album.
     *
     * @param int         $id Album ID
     *
     * @param Albums|null $album
     *
     * @return int
     */
    public function selectSongsNumber(int $id, Albums $album = null): int
    {
        $this->executeQuery(
            'SELECT COUNT(id_song) as tracks_number FROM songs WHERE album_id = :id',
            [':id' => $id]
        );

        $record = $this->fetchOne();
        if ($album) $album->hydrate($record);

        return $record['tracks_number'] ?? false;
    }

    /**
     * Return the genre of the album.
     * If an album is passed as parameter, the genre is assigned directly to the album.
     *
     * @param int         $id Album ID
     *
     * @param Albums|null $album
     *
     * @return string
     */
    public function selectAlbumGenre(int $id, Albums $album = null): string
    {
        $this->executeQuery(
            '
                SELECT genres.name as genre, COUNT(*) AS magnitude
                FROM songs
                    INNER JOIN genres ON songs.genre_id = genres.id_genre
                WHERE songs.album_id = :id
                GROUP BY genres.name
                ORDER BY magnitude DESC
            ',
            [':id' => $id]
        );

        $record = $this->fetchOne();
        if ($album) $album->hydrate($record);

        return $record['genre'] ?? false;
    }

    /**
     * Select all songs from album
     *
     * @param int $id
     *
     * @return Songs[]
     */
    public function selectAlbumSongs(int $id): array
    {
        $this->executeQuery(
            '
                SELECT id_song, title, duration, track_number
                FROM songs
                WHERE album_id = :id
                ORDER BY track_number
            ',
            [':id' => $id]
        );

        $songs = [];
        foreach ($this->fetchRecords() as $record)
            $songs[] = EntityFactory::createEntity('songs', $record);

        return $songs;
    }

    /**
     * Search albums relative to the search term.
     * Search format -> ... LIKE %searchTerm% ...
     *
     * @param string $searchTerm
     *
     * @return Albums[]
     */
    public function searchAlbums(string $searchTerm): array
    {
        $this->executeQuery(
            'SELECT id_album, name, image FROM albums WHERE name LIKE :term',
            [':term' => "%$searchTerm%"]
        );

        $albums = [];
        foreach ($this->fetchRecords() as $record)
            $albums[] = EntityFactory::createEntity('albums', $record);

        return $albums;
    }
}
