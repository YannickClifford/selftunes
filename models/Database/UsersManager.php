<?php
/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - UsersManager.php
 * Description  :   [Description]
 *
 * Created      :   29.07.2019
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

namespace SelfTunes\Models\Database;

use SelfTunes\Models\Database\Entity\EntityFactory;
use SelfTunes\Models\Database\Entity\Playlists;
use SelfTunes\Models\Database\Entity\Users;

class UsersManager extends DatabaseManager
{

    /**
     * Insert the user in the database.
     *
     * @param array $userInfos Need to respect the format :<br>
     *                         $userInfos['userEmail'],<br>
     *                         $userInfos['userFName'],<br>
     *                         $userInfos['userLName'],<br>
     *                         $userInfos['userPsw'].
     *
     * @return bool
     */
    public function registerNewAccount(array $userInfos): bool
    {
        $userInfos['userPsw'] = password_hash($userInfos['userPsw'], PASSWORD_DEFAULT);

        return $this->executeQuery(
            '
                INSERT INTO users (email, firstname, lastname, password) 
                VALUES (:userEmail, :userFName, :userLName, :userPsw)
            ',
            $userInfos
        );
    }

    /**
     * Check if the user infos are matching with the database.
     *
     * @param string $userEmail
     * @param string $userPassword
     *
     * @return bool
     */
    public function checkLogin(string $userEmail, string $userPassword): bool
    {
        $this->executeQuery(
            'SELECT password FROM users WHERE email = :email',
            [':email' => $userEmail]
        );

        return password_verify($userPassword, $this->fetchOne()['password']);
    }

    /**
     * Select the Id of the user from his email address.
     *
     * @param string $userEmail
     *
     * @return int
     */
    public function selectUserId(string $userEmail): int
    {
        $this->executeQuery('SELECT id_user FROM users WHERE email = :email', [':email' => "$userEmail"]);
        $record = $this->fetchOne();

        return isset($record['id_user']) ? $record['id_user'] : 0;
    }

    /**
     * Select all playlists of the user.
     *
     * @param int $userId
     *
     * @return Playlists[]
     * @uses EntityFactory
     */
    public function selectUserPlaylists(int $userId): array
    {
        $this->executeQuery(
            'SELECT id_playlist, name FROM playlists WHERE user_id=:userId',
            [':userId' => $userId]
        );

        $playlists = [];
        foreach ($this->fetchRecords() as $record)
            $playlists[] = EntityFactory::createEntity('playlists', $record);

        return $playlists;
    }

    /**
     * Select one user.
     *
     * @param int $userId
     *
     * @return Users
     */
    public function selectOne(int $userId): Users
    {
        $this->executeQuery('SELECT * FROM users WHERE id_user=?', [$userId]);
        $record = $this->fetchOne();

        return EntityFactory::createEntity('users', $record);
    }

    public function updateUser(array $userInfo): bool
    {
        if (empty($userInfo['pwd']))
            $userInfo['pwd'] = $this->selectPassword($userInfo['id']);
        else
            $userInfo['pwd'] = password_hash($userInfo['pwd'], PASSWORD_DEFAULT);

        return $this->executeQuery(
            '
                UPDATE IGNORE users
                SET email=:email, firstname=:fname, lastname=:lname, password=:pwd
                WHERE id_user=:id
            ',
            $userInfo
        );
    }

    /**
     * Select the password of the user.
     *
     * @param int $id
     *
     * @return string the password hashed of the user. Empty string if there is no match.
     */
    public function selectPassword(int $id): string
    {
        $this->executeQuery('SELECT password FROM users WHERE id_user=?', [$id]);
        $user = $this->fetchOne();

        return isset($user['password']) ? $user['password'] : '';
    }
}
