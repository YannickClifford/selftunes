<?php
/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - artistsManager.php
 * Description  :   [Description]
 *
 * Created      :   30.07.2019
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

namespace SelfTunes\Models\Database;

use SelfTunes\Models\Database\Entity\Albums;
use SelfTunes\Models\Database\Entity\Artists;
use SelfTunes\Models\Database\Entity\EntityFactory;
use SelfTunes\Models\Database\Entity\Songs;


class ArtistsManager extends DatabaseManager
{

    public const TABLE_NAME = 'Artists';

    /**
     * Select one artist.
     *
     * @param int $id
     *
     * @return Artists
     */
    public function selectOne(int $id): Artists
    {
        $this->executeQuery('SELECT id_artist, name FROM artists WHERE id_artist = :id', [':id' => $id]);

        $record = $this->fetchOne();

        return EntityFactory::createEntity(self::TABLE_NAME, $record);
    }

    /**
     * Select all albums from artist.
     *
     * @param int $id
     *
     * @return Albums[]
     */
    public function selectArtistAlbums(int $id): array
    {
        $this->executeQuery(
            '
                SELECT id_album, name, year, image
                FROM albums
                WHERE artist_id = :id
                ORDER BY year
            ',
            [':id' => $id]
        );

        $albums = [];
        foreach ($this->fetchRecords() as $record)
            $albums[] = EntityFactory::createEntity('albums', $record);

        return $albums;
    }

    /**
     * Select all songs of the artist.
     * If $popular is true, select only the 5 more listened songs.
     *
     * @param int  $id Id of the artist
     * @param bool $populars
     *
     * @return Songs[]
     */
    public function selectArtistSongs(int $id, $populars = false): array
    {
        $sort = $populars ? 'ORDER BY s.plays DESC LIMIT 5' : 'ORDER BY al.year DESC, al.id_album, s.track_number';
        $this->executeQuery(
            '
                SELECT s.id_song, s.album_id, s.title, s.duration, s.path, al.name as album_name
                FROM songs as s
                    INNER JOIN albums al on s.album_id = al.id_album
                    INNER JOIN artists ar on al.artist_id = ar.id_artist
                WHERE ar.id_artist = :id
                ' . $sort . '
            ',
            [':id' => $id]
        );

        $songs = [];
        $row = 1;
        foreach ($this->fetchRecords() as $record) {
            $record['row'] = $row;
            $songs[] = EntityFactory::createEntity('songs', $record);
            $row++;
        }

        return $songs;
    }

    /**
     * Search artists relative to the search term.
     * Search format -> ... LIKE %searchTerm% ...
     *
     * @param string $searchTerm
     *
     * @return Artists[]
     */
    public function searchArtists(string $searchTerm): array
    {
        $this->executeQuery(
            'SELECT id_artist, name FROM artists WHERE name LIKE :term',
            [':term' => "%$searchTerm%"]
        );

        $artists = [];
        foreach ($this->fetchRecords() as $record)
            $artists[] = EntityFactory::createEntity('artists', $record);

        return $artists;
    }
}
