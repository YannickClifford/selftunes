<?php
/**
 * Author       :   Yannick.BAUDRAZ@cpnv.ch
 * Project      :   SelfTunes - DatabaseManager.php
 * Description  :   DatabaseManager class
 *
 * Created      :   12.07.2019
 * Updates      :   21.07.2019
 *                      Constructor calls setter
 *                      executeQuery method doesn't fetch anymore, fetchRecords does it
 *
 * Git source   :   https://bitbucket.org/YannickClifford/selftunes/src/master/models/Database/DatabaseManager.php
 *
 * Created with PhpStorm.
 */

namespace SelfTunes\Models\Database;

use PDO;
use PDOException;
use PDOStatement;

require_once dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'config/config_database.php';

/**
 * Class DatabaseManager
 *
 * @package models
 */
class DatabaseManager
{

    /**
     * Data Source Name,
     * contains the information required to connect to the database.
     *
     * @var string
     */
    private const DSN =
        DB_SQL_DRIVER .
        ':host=' . DB_HOSTNAME .
        ';dbname=' . DB_NAME .
        ';port=' . DB_PORT .
        ';charset=' . DB_CHARSET;

    /**
     * @var PDO
     */
    protected $dbConnection;

    /**
     * @var PDOStatement
     */
    private $statement;

    /**
     * DatabaseManager constructor.
     */
    public function __construct() { $this->setDbConnection(); }

    /**
     * DatabaseManager destructor.
     * Close the connection
     */
    public function __destruct() { $this->closeConnection(); }

    /**
     * Close the connection with the database.
     */
    public function closeConnection() { $this->dbConnection = null; }

    /**
     * Execute a query received as parameter.
     *
     * @param string     $query Must be correctly build for sql syntax.
     * @param array|null $queryArray
     *
     * @return bool True if the query is ok, otherwise false.
     */
    public function executeQuery(string $query, array $queryArray = null): bool
    {
        $queryResult = false;

        if ($this->dbConnection) {
            $this->statement = $this->dbConnection->prepare($query);
            $queryResult = $this->statement->execute($queryArray);
        }

        return $queryResult;
    }

    /**
     * Returns the result of an executed query.
     *
     * @return array
     */
    public function fetchRecords(): array { return $this->statement->fetchAll(); }

    /**
     * Return a single row of an executed query.
     *
     * @return array|false
     */
    public function fetchOne() { return $this->statement->fetch(); }

    /**
     * Returns the ID of the last inserted row.
     *
     * @return string
     */
    public function getLastInsertId(): string { return $this->dbConnection->lastInsertId(); }

    /**
     * Setter of $dbConnection
     */
    public function setDbConnection(): void
    {
        if ($this->dbConnection)
            $this->closeConnection();

        try {
            $this->dbConnection = new PDO(self::DSN, DB_USER_NAME, DB_USER_PWD);
            $this->dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->dbConnection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        } catch (PDOException $exception) {
            die($exception->getMessage());
        }
    }
}
