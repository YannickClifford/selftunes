<?php
/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - SongsManager.php
 * Description  :   [Description]
 *
 * Created      :   25.07.2019
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   https://bitbucket.org/YannickClifford/selftunes/src/master/models/Database/SongsMAnager.php
 *
 * Created with PhpStorm.
 */

namespace SelfTunes\Models\Database;

use SelfTunes\Models\Database\Entity\EntityFactory;
use SelfTunes\Models\Database\Entity\Songs;


class SongsManager extends DatabaseManager
{

    public const TABLE_NAME = 'Songs';

    /**
     * Select 10 random songs when a page is loaded without AJAX.
     *
     * @return Songs[]
     */
    public function selectForStartApp(): array
    {
        $this->executeQuery('SELECT id_song FROM songs ORDER BY RAND() LIMIT 10');

        $songs = [];
        foreach ($this->fetchRecords() as $record)
            $songs[] = EntityFactory::createEntity(self::TABLE_NAME, $record);

        return $songs;
    }

    /**
     * Select one song for AJAX request.
     *
     * @param int $id
     *
     * @return Songs
     */
    public function selectOneForAJAX(int $id): Songs
    {
        $this->executeQuery(
            '
                SELECT songs.*, artists.name as artist_name, albums.image as album_image
                FROM songs
                    INNER JOIN albums on songs.album_id = albums.id_album
                    INNER JOIN artists on albums.artist_id = artists.id_artist
                WHERE id_song = :id
            ',
            [':id' => $id]
        );

        $record = $this->fetchOne();
        if (isset($record['album_image']))
            $record['album_image'] = !empty($record['album_image'])
                ? DIR_ALBUMS_IMAGES . '/' . $record['album_image']
                : GENERIC_ALBUM_PATH;

        return EntityFactory::createEntity(self::TABLE_NAME, $record);
    }

    /**
     * Update plays by incrementing by one.
     *
     * @param int $id
     *
     * @return bool
     */
    public function updatePlays(int $id): bool
    {
        return $this->executeQuery(
            'UPDATE songs SET plays = plays + 1 WHERE id_song = :id',
            [":id" => $id]
        );
    }

    /**
     * Search songs relative to the search term.
     * Search format -> ... LIKE %searchTerm% ...
     *
     * @param string $searchTerm
     *
     * @return Songs[]
     */
    public function searchSongs(string $searchTerm): array
    {
        $this->executeQuery(
            '
                SELECT songs.id_song, songs.title, songs.duration, songs.album_id,
                       albums.artist_id, albums.name as album_name,
                       artists.name as artist_name
                FROM songs
                    INNER JOIN albums on songs.album_id = albums.id_album
                    INNER JOIN artists on albums.artist_id = artists.id_artist
                WHERE title LIKE :term
                ORDER BY RAND()
                LIMIT 10
            ',
            [':term' => "%$searchTerm%"]
        );

        $songs = [];
        $row = 1;
        foreach ($this->fetchRecords() as $record) {
            $record['row'] = $row;
            $songs[] = EntityFactory::createEntity('songs', $record);
            $row++;
        }

        return $songs;
    }
}
