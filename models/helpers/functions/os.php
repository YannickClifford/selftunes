<?php
/**
 * Author       :   Yannick.BAUDRAZ@cpnv.ch
 * Project      :   SelfTunes - os.php
 * Description  :   Function for os system, files, folders etc.
 *
 * Created      :   10.07.2019
 * Updates      :   17.07.2019 - Yannick.BAUDRAZ@cpnv.ch
 *                      Change packet size to 70KB
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

/**
 * Delete a directory recursively
 *
 * @param string $directory
 *
 * @return bool The result of the deletion
 */
function delTree($directory)
{
    $files = array_diff(scandir($directory), ['.', '..']);
    foreach ($files as $file) {
        (is_dir("$directory/$file")) ? delTree("$directory/$file") : unlink("$directory/$file");
    }

    return rmdir($directory);
}

/**
 * Get meta tags from a in file base in a remote server.
 *
 * @param string $remoteFile Path of the file.
 *
 * @return array|bool
 *
 * @package getid3_lib
 * @package getID3
 */
function remoteFileInfo($remoteFile)
{
    //region Create Temporary file
    $uniqueId = uniqid("selftunes", true);
    $dirName = 'temp';
    $fileTmp = $dirName . DIRECTORY_SEPARATOR . $uniqueId;
    if (!is_dir($dirName)) mkdir($dirName);
    //endregion

    //region Open files resource
    $fileResource = fopen($fileTmp, "x");
    $urlResource = fopen($remoteFile, "r");
    if (!$fileResource || !$urlResource) return false;
    //endregion

    //region Take bytes of files
    $kbGot = 0;
    $kbToGet = 71680; // 70 KB
    $packetSize = 1024; // 1 KB
    $beginningOfFile = null;

    //region First 70 KB of the file
    while (!feof($urlResource) && $kbGot < $kbToGet) {
        $beginningOfFile .= fgets($urlResource, $packetSize);
        $kbGot += $packetSize;
    }
    fwrite($fileResource, $beginningOfFile);
    //endregion

    //region Get header file
    $sessionSizeFile = curl_init($remoteFile);
    curl_setopt($sessionSizeFile, CURLOPT_HEADER, true);
    curl_setopt($sessionSizeFile, CURLOPT_NOBODY, true);
    curl_setopt($sessionSizeFile, CURLOPT_RETURNTRANSFER, true);
    $header = curl_exec($sessionSizeFile);
    curl_close($sessionSizeFile);
    //endregion

    //region Get size of remote file
    $size = 0;
    if (preg_match('/Content-Length: (\d+)/', $header, $matches)) {
        $contentLength = (int)$matches[1];
        $size = $contentLength;
    }
    //endregion

    //region Last 70 KB of the file
    $endOfFile = null;
    if ($size > $kbToGet) {
        $sessionInfoFile = curl_init($remoteFile);
        curl_setopt($sessionInfoFile, CURLOPT_FILE, $fileResource);
        curl_setopt($sessionInfoFile, CURLOPT_HEADER, 0);
        curl_setopt($sessionInfoFile, CURLOPT_RESUME_FROM, $size - $kbToGet);
        $endOfFile .= curl_exec($sessionInfoFile);
        curl_close($sessionInfoFile);
        fwrite($fileResource, $endOfFile);
    }
    //endregion
    //endregion

    //region Close resources
    @fclose($fileResource);
    @fclose($urlResource);
    //endregion

    //region Get file infos
    try {
        $getId3 = new getID3;
        $filename = $fileTmp;
        $fileInfo = $getId3->analyze($filename);
        getid3_lib::CopyTagsToComments($fileInfo);
        $fileInfo['realSize'] = $size;
    } catch (Exception $exception) {
        $fileInfo = false;
    } finally {
        unlink($fileTmp);
        delTree($dirName);
    }
    //endregion

    return $fileInfo;
}

