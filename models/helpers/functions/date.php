<?php
/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - date.php
 * Description  :   [Description]
 *
 * Created      :   08.08.2019
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

/**
 * Change the format of the date passed in parameter
 *
 * @param string $date   Date of the playlist.<br>
 *                       A date/time string. Valid formats are explained in <a
 *                       href="https://www.php.net/manual/en/datetime.formats.php">Date and Time Formats</a>.
 * @param string $format Format accepted by strftime() native function.
 *
 * @return string The date with the new format.
 */
function changeDateFormat(string $date, string $format): string
{
    try {
        $dateTime = new DateTime($date);
        $newDate = strftime($format, $dateTime->getTimestamp());
        return $newDate;
    } catch (Exception $e) {
        die($e->getMessage());
    }
}
