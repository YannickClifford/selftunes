<?php
/**
 * Author       :   Yannick.BAUDRAZ@cpnv.ch
 * Project      :   SelfTunes - songs_insert_database.php
 * Description  :   [Description]
 *
 * Created      :   10.07.2019
 * Updates      :   [dd.mm.yyyy author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

use SelfTunes\Models\{Database\DatabaseManager, Ftp\Ftp};

require '../../../config/config.php';
require '../../../config/config_ftp.php';
require '../../../config/config_database.php';
require '../../../vendor/autoload.php';
require '../../helpers/functions/os.php';

$needImage = true; // true if we need to download the image in the website

$rootDir = dirname(dirname(__DIR__));
$albumsImagesDir = $rootDir . '/views/assets/images/albums';

//region Remote Server (FTP)
$ftp = new Ftp();
$ftp->connect();
$ftp->login();
$ftp->setPassive();
$directories = $ftp->recurseFileList('/');

//region Files info
$urlConnection = 'ftp://' . FTP_USERNAME . ':' . FTP_PASSWORD . '@' . FTP_HOST;
$directoriesTags = [];
foreach ($directories as $directoryPath => $directory) {
    if ($directoryPath !== '/') {
        $arrayTemp = [];
        foreach ($directory as $fileInServer) {
            $remoteFileUrl = $urlConnection . $directoryPath . '/' . $fileInServer;
            if (!is_dir($remoteFileUrl)) {
                $fileWithInfo = remoteFileInfo($remoteFileUrl);
                $fileWithInfo['path'] = substr($directoryPath . '/' . basename($remoteFileUrl), 1);
                if (isset($fileWithInfo['mime_type']) && strpos($fileWithInfo['mime_type'], 'image') !== false && $needImage) {
                    $localPath = $albumsImagesDir . '/' . basename($remoteFileUrl);
                    $ftp->downloadFile($fileWithInfo['path'], $localPath);
                } else {
                    $tmpDir = explode('/', $directoryPath);
                    $dirName = end($tmpDir);
                    if (isset($fileWithInfo['comments']['title']) && isset($fileWithInfo['comments']['album']))
                        $fileWithInfo['imagePath'] = $dirName . '.jpg';
                    elseif (isset($fileWithInfo['comments']['artist']) && !isset($fileWithInfo['comments']['album'])) {
                        $fileWithInfo['imagePath'] = '';
                        $fileWithInfo['comments']['album'] = $fileWithInfo['comments']['title'];
                    }
                    array_push($arrayTemp, $fileWithInfo);
                }
            }
        }
        $directoriesTags[] = $arrayTemp;
    }
}
//endregion
$ftp->close();
//endregion

//region Remove empty arrays in directory array
foreach ($directoriesTags as $index => $directory)
    if (count($directoriesTags[$index]) == 0)
        unset($directoriesTags[$index]);
//endregion

$dbManager = new DatabaseManager();
foreach ($directoriesTags as $directory) {
    $artistInserted = false;
    $albumInserted = false;
    $genreInserted = false;

    foreach ($directory as $file) {
        if (!isset($file['error']) || isset($file['id3v2'])) {
            if (strpos($file['mime_type'], 'audio') !== false) {


                //region Artist
                if (!$artistInserted) {
                    $artist = $file['comments']['artist'][0];
                    $dbManager->executeQuery("Select * from artists");
                    $artists = $dbManager->fetchRecords();
                    if (($indexArtist = array_search($artist, array_column($artists, 'name'))) === false) {
                        $dbManager->executeQuery("INSERT INTO artists (name) VALUES (:artist)", [':artist' => $artist]);
                        $artistId = $dbManager->getLastInsertId();
                    } else {
                        $artistId = $artists[$indexArtist]['id_artist'];
                    }
                }
                //endregion

                //region Album
                if (!$albumInserted) {
                    $dbManager->executeQuery("SELECT id_album, name FROM albums where artist_id = :artistID", [":artistID" => $artistId]);
                    $albums = $dbManager->fetchRecords();
                    $album = $file['comments']['album'][0];
                    if (($indexAlbum = array_search($album, array_column($albums, 'name'))) === false) {
                        $date = $file['comments']['date'][0] ?? $file['comments']['year'][0];
                        $image = $file['imagePath'];
                        $queryInsertAlbum = "
                            INSERT INTO albums (artist_id, name, year, image) 
                            VALUES (:artistID, :album, :year, :image)
                        ";
                        $queryArray = [
                            ':artistID' => $artistId,
                            ':album' => $album,
                            ':year' => $date,
                            ':image' => $image,
                        ];
                        $dbManager->executeQuery($queryInsertAlbum, $queryArray);
                        $albumId = $dbManager->getLastInsertId();
                        $albumInserted = true;
                    } else {
                        $albumId = $albums[$indexAlbum]['id_album'];
                    }
                }
                //endregion

                //region Genre
                $genre = $file['comments']['genre'][0];
                // Insert only if the genre is not in the database
                $dbManager->executeQuery("Select * from genres");
                $genres = $dbManager->fetchRecords();
                if (($indexGenre = array_search($genre, array_column($genres, 'name'))) === false) {
                    $dbManager->executeQuery("INSERT INTO genres (name) VALUES (:genre)", [':genre' => $genre]);
                    $genreId = $dbManager->getLastInsertId();
                } else {
                    $genreId = $genres[$indexGenre]['id_genre'];
                }
                //endregion

                //region Insert songs
                $path = $file['path'];
                $dbManager->executeQuery("SELECT path FROM songs WHERE path = '$path'");
                $songInDatabase = $dbManager->fetchRecords();
                if (!$songInDatabase) {
                    $title = $file['comments']['title'][0];
                    $trackNumber = (int)$file['comments']['track_number'][0];
                    $seconds = $file['mime_type'] == 'audio/mpeg' ? (($file['realSize'] / $file['bitrate']) * 8) : $file['playtime_seconds'];
                    $duration = gmdate("H:i:s", ($seconds));

                    $queryInsertSongs = "
                        INSERT INTO songs (album_id, genre_id, title, duration, path, track_number)
                        VALUES (:albumID, :genreID, :title, :duration, :path, :trackNumber)
                    ";
                    $queryArray = [
                        ':albumID' => $albumId,
                        ':genreID' => $genreId,
                        ':title' => $title,
                        ':duration' => $duration,
                        ':path' => $path,
                        ':trackNumber' => $trackNumber,
                    ];
                    $dbManager->executeQuery($queryInsertSongs, $queryArray);
                }
                //endregion
            }
        }
    }
}

$dbManager->closeConnection();
