<?php
/**
 * Author       :   Yannick.BAUDRAZ@cpnv.ch
 * Project      :   SelfTunes - rename_songs-filename.php
 * Description  :   Rename filename of the songs in the folder in the remote server
 *
 * Created      :   16.07.2019
 * Updates      :   [dd.mm.yyyy author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

use SelfTunes\Models\Ftp\Ftp;

require '../../../config/config_ftp.php';
require '../../../vendor/autoload.php';
require '../../helpers/functions/os.php';

$ftp = new Ftp();
$ftpConnection = $ftp->connect();
$ftp->login();
$ftp->setPassive();
$directories = $ftp->recurseFileList('/');

$urlConnection = 'ftp://' . FTP_USERNAME . ':' . FTP_PASSWORD . '@' . FTP_HOST;
foreach ($directories as $directoryPath => $directory) {
    if ($directoryPath !== '/') {
        foreach ($directory as $fileInServer) {
            $remoteFileUrl = $urlConnection . $directoryPath . DIRECTORY_SEPARATOR . $fileInServer;
            if (!is_dir($remoteFileUrl)) {
                $specialChars = ["?", "[", "]", "/", "\\", "=", "<", ">", ":", ";", ",", "'", "\"", "&", "$", "#", "*", "(", ")", "|", "~", "`", "!", "{", "}"];
                $withoutSpecials = str_replace($specialChars, '', $fileInServer);
                $newFileName = mb_strtolower(str_replace(' ', '_', $withoutSpecials), 'UTF-8');
                if (!file_exists($urlConnection . $directoryPath . DIRECTORY_SEPARATOR . $newFileName))
                    ftp_rename($ftpConnection, $directoryPath . DIRECTORY_SEPARATOR . $fileInServer, $directoryPath . DIRECTORY_SEPARATOR . $newFileName);
            }
        }
    }
}

$ftp->close();
