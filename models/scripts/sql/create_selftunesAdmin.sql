/**
  MySQL script for the generation of the user `selftunesAdmin`

  Project   : SelfTunes
  Name      : create_selftunesAdmin.sql
  Author    : Yannick.BAUDRAZ@cpnv.ch
  Date      : 09.07.2019
 */


CREATE USER 'selftunesAdmin'@'localhost' IDENTIFIED BY 'SelfTunes-CPNV';
GRANT USAGE ON *.* TO 'selftunesAdmin'@'localhost';
GRANT ALL ON `selftunes`.* TO 'selftunesAdmin'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;
