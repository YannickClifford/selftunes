/**
  MySQL script for the generation of the database `selftunes`

  Project : SelfTunes
  Name :    create_database.sql
  Author :  Yannick.BAUDRAZ@cpnv.ch
  Date :    09.07.2019
  Updates : 09.07.2019 - Yannick.BAUDRAZ@cpnv.ch
                Add property `track_number` in `Songs` table
            15.07.2019 - Yannick.BAUDRAZ@cpnv.ch
                Add unique constraint for genre name and artist name
                `Artists` is now connected with `Albums`
            17.07.2019 - Yannick.BAUDRAZ@cpnv.ch
                Add unique constraint for email user, all paths
                Update length of album name to 60 char
            07.08.2019 - Yannick.BAUDRAZ@cpnv.ch
                Change column `playlists.date_creation` type from DATE to DATETIME
            12.08.2019
                Add column `songs.plays`
 */


SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE =
        'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema selftunes
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `selftunes`;

-- -----------------------------------------------------
-- Schema selftunes
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `selftunes` DEFAULT CHARACTER SET utf8;
USE `selftunes`;

-- -----------------------------------------------------
-- Table `selftunes`.`Users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `selftunes`.`Users`;

CREATE TABLE IF NOT EXISTS `selftunes`.`Users`
(
    `id_user`           INT          NOT NULL AUTO_INCREMENT,
    `email`             VARCHAR(255) NOT NULL,
    `firstname`         VARCHAR(40)  NULL,
    `lastname`          VARCHAR(40)  NULL,
    `password`          VARCHAR(255) NOT NULL,
    `date_registration` DATE         NOT NULL DEFAULT (now()),
    `profile_picture`   VARCHAR(255) NULL,
    PRIMARY KEY (`id_user`),
    UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE,
    UNIQUE INDEX `profile_picture_UNIQUE` (`profile_picture` ASC) VISIBLE
)
    ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `selftunes`.`Playlists`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `selftunes`.`Playlists`;

CREATE TABLE IF NOT EXISTS `selftunes`.`Playlists`
(
    `id_playlist`   INT          NOT NULL AUTO_INCREMENT,
    `user_id`       INT          NOT NULL,
    `name`          VARCHAR(100) NOT NULL,
    `date_creation` DATETIME     NOT NULL DEFAULT (now()),
    PRIMARY KEY (`id_playlist`),
    INDEX `fk_Playlists_Users_idx` (`user_id` ASC) VISIBLE,
    CONSTRAINT `fk_Playlists_Users`
        FOREIGN KEY (`user_id`)
            REFERENCES `selftunes`.`Users` (`id_user`)
            ON DELETE CASCADE
            ON UPDATE CASCADE
)
    ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `selftunes`.`Genres`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `selftunes`.`Genres`;

CREATE TABLE IF NOT EXISTS `selftunes`.`Genres`
(
    `id_genre` INT         NOT NULL AUTO_INCREMENT,
    `name`     VARCHAR(30) NOT NULL,
    PRIMARY KEY (`id_genre`),
    UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE
)
    ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `selftunes`.`Artists`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `selftunes`.`Artists`;

CREATE TABLE IF NOT EXISTS `selftunes`.`Artists`
(
    `id_artist` INT         NOT NULL AUTO_INCREMENT,
    `name`      VARCHAR(50) NOT NULL,
    PRIMARY KEY (`id_artist`),
    UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE
)
    ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `selftunes`.`Albums`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `selftunes`.`Albums`;

CREATE TABLE IF NOT EXISTS `selftunes`.`Albums`
(
    `id_album`  INT          NOT NULL AUTO_INCREMENT,
    `artist_id` INT          NOT NULL,
    `name`      VARCHAR(60)  NOT NULL,
    `year`      YEAR(4)      NOT NULL,
    `image`     VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id_album`),
    INDEX `fk_Albums_Artists1_idx` (`artist_id` ASC) VISIBLE,
    UNIQUE INDEX `image_UNIQUE` (`image` ASC) VISIBLE,
    CONSTRAINT `fk_Albums_Artists1`
        FOREIGN KEY (`artist_id`)
            REFERENCES `selftunes`.`Artists` (`id_artist`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `selftunes`.`Songs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `selftunes`.`Songs`;

CREATE TABLE IF NOT EXISTS `selftunes`.`Songs`
(
    `id_song`      INT          NOT NULL AUTO_INCREMENT,
    `genre_id`     INT          NOT NULL,
    `album_id`     INT          NOT NULL,
    `title`        VARCHAR(150) NOT NULL,
    `duration`     TIME         NOT NULL,
    `path`         VARCHAR(255) NOT NULL,
    `track_number` INT(2)       NOT NULL,
    `plays`        INT          NOT NULL DEFAULT 0,
    PRIMARY KEY (`id_song`),
    UNIQUE INDEX `path_UNIQUE` (`path` ASC) VISIBLE,
    INDEX `fk_Songs_Genre1_idx` (`genre_id` ASC) VISIBLE,
    INDEX `fk_Songs_Albums1_idx` (`album_id` ASC) VISIBLE,
    CONSTRAINT `fk_Songs_Genre1`
        FOREIGN KEY (`genre_id`)
            REFERENCES `selftunes`.`Genres` (`id_genre`)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    CONSTRAINT `fk_Songs_Albums1`
        FOREIGN KEY (`album_id`)
            REFERENCES `selftunes`.`Albums` (`id_album`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `selftunes`.`Songs_Playlist`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `selftunes`.`Songs_Playlist`;

CREATE TABLE IF NOT EXISTS `selftunes`.`Songs_Playlist`
(
    `playlist_id` INT NOT NULL,
    `song_id`     INT NOT NULL,
    PRIMARY KEY (`song_id`, `playlist_id`),
    INDEX `fk_Playlists_has_Songs_Songs1_idx` (`song_id` ASC) VISIBLE,
    INDEX `fk_Playlists_has_Songs_Playlists1_idx` (`playlist_id` ASC) VISIBLE,
    CONSTRAINT `fk_Playlists_has_Songs_Playlists1`
        FOREIGN KEY (`playlist_id`)
            REFERENCES `selftunes`.`Playlists` (`id_playlist`)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    CONSTRAINT `fk_Playlists_has_Songs_Songs1`
        FOREIGN KEY (`song_id`)
            REFERENCES `selftunes`.`Songs` (`id_song`)
            ON DELETE CASCADE
            ON UPDATE CASCADE
)
    ENGINE = InnoDB;

SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;
