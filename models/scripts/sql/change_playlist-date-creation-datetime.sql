/**
  MySQL script : Change the type of the column `playlists.date_creation` from DATE to DATETIME.

  Project : SelfTunes
  Name :    change_playlist-date-creation-datetime.sql
  Author :  Yannick Baudraz <Yannick.BAUDRAZ@cpnv.ch>
  Date :    07.08.2019
 */

USE selftunes;

ALTER TABLE playlists
    MODIFY date_creation DATETIME NOT NULL DEFAULT (NOW());
