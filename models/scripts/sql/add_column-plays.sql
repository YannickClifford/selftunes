/**
  MySQL script : Adding the column plays in the table `songs` of the database SelfTunes.

  Project : SelfTunes
  Name :    add_column-play.sql
  Author :  Yannick Baudraz <Yannick.BAUDRAZ@cpnv.ch>
  Date :    24.07.2019
 */

use selftunes;

ALTER TABLE songs
    ADD COLUMN plays INT NOT NULL DEFAULT 0;
