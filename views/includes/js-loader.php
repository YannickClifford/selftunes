<?php
/**
 * Author       :   Yannick.BAUDRAZ@cpnv.ch
 * Project      :   SelfTunes - js-loader.php
 * Description  :   [Description]
 *
 * Created      :   20.07.2019
 * Updates      :   [dd.mm.yyyy author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */
?>

<!-- Core -->
<script src="views/assets/scripts/js/plugins/jquery.min.js"></script>
<script src="views/assets/scripts/js/plugins/popper.min.js"></script>
<script src="views/assets/scripts/js/plugins/bootstrap.min.js"></script>

<!-- BLK• JS -->
<script src="views/assets/scripts/js/themes/blk-design-system.min.js"></script>

<!-- Plugins -->
<script src="views/assets/scripts/js/plugins/moment.min.js"></script>
<script src="views/assets/scripts/js/plugins/jquery.caret.js"></script>
<script src="views/assets/scripts/js/plugins/jasny-bootstrap.min.js"></script>

<!-- Custom -->
<script src="views/assets/scripts/js/custom/custom.js" type="module" charset="utf-8"></script>
<?php if ($_GET['action'] != "login" && $_GET['action'] != "register"): ?>
    <script>
        /**
         * Song to send to JS
         *
         * @typedef SongsForJS
         * @type {Array}
         * @property {number} id_song
         * @property {number} genre_id
         * @property {number} album_id
         * @property {string} title
         * @property {string} duration
         * @property {string} path
         * @property {number} track_number
         * @property {number} plays
         *
         * @var {[SongsForJS]} objectOfSongs
         */
        if (typeof arrayOfSongs === 'undefined')
            arrayOfSongs = <?= $_SESSION['start-trackList'] ?? null ?>;
    </script>
    <script src="views/assets/scripts/js/custom/music-player.js" type="module" charset="utf-8"></script>
    <script src="views/assets/scripts/js/custom/playlist.js" type="module" charset="utf-8"></script>
    <script src="views/assets/scripts/js/custom/pages-ajax.js" type="module" charset="utf-8"></script>
<?php endif ?>
