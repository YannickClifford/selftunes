<?php
/**
 * Author       :   Yannick.BAUDRAZ@cpnv.ch
 * Project      :   SelfTunes - nav-bar.php
 * Description  :   [Description]
 *
 * Created      :   19.07.2019
 * Updates      :   [dd.mm.yyyy author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */
?>

<div id="nav-bar" class="col-4 col-md-3 col-xl-2">
    <nav class="nav-bar">
        <div class="d-flex justify-content-around">
            <a href="index.php" class="logo">
                <img src="views/assets/images/logo.png" alt="Logo">
            </a>
            <ul class="nav nav-pills nav-pills-just-icons nav-pills-neutral flex-column mt-3 pb-2"
                id="search-btn-container">
                <li class="nav-item mx-auto search-item">
                    <a class="nav-link <?php if (($_GET['action'] == 'search')) echo 'active show' ?>" data-toggle="tab"
                       href="index.php?action=search">
                        <i class="tim-icons icon-zoom-split mb-1"></i> Rechercher
                    </a>
                </li>
            </ul>
        </div>

        <ul id="nav-pills-container" class="nav nav-pills nav-pills-primary flex-column mt-3">
            <li class="nav-item">
                <a class="nav-link <?php if (($_GET['action'] == 'home')) echo 'active show' ?>"
                   data-toggle="tab" href="index.php?action=home">Accueil</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php if (($_GET['action'] == 'playlists')) echo 'active show' ?>"
                   data-toggle="tab" href="index.php?action=playlists">Playlists</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php if (($_GET['action'] == 'account')) echo 'active show' ?>" data-toggle="tab"
                   href="index.php?action=account">Compte</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php?action=logout">Déconnexion</a>
            </li>
        </ul>
    </nav>
</div>
