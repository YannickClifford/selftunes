<?php
/**
 * Author       :   Yannick.BAUDRAZ@cpnv.ch
 * Project      :   SelfTunes - head.php
 * Description  :   For meta and link
 *
 * Created      :   07.07.2019
 * Updates      :   [dd.mm.yyyy author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */
?>

<!-- Meta Tags -->
<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet">

<!-- Icons -->
<link href="views/assets/styles/css/nucleo-icons.css" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">

<!-- Plugins -->
<link href="views/assets/styles/css/jasny-bootstrap.css" rel="stylesheet">

<!-- Theme CSS -->
<link href="views/assets/styles/css/blk-design-system.css" rel="stylesheet" type="text/css">

<!-- SelfTunes CSS -->
<link href="views/assets/styles/css/selftunes.css" rel="stylesheet" type="text/css">
