<?php
/**
 * Author       :   Yannick.BAUDRAZ@cpnv.ch
 * Project      :   SelfTunes - playing-bar.php
 * Description  :   Playing bar at the bottom of the page.
 *
 * Created      :   17.07.2019
 * Updates      :   [dd.mm.yyyy author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */
?>

<footer>
    <div id="playing-bar_container">
        <div id="playing-bar">
            <!--region Playing Bar Left-->
            <div id="playing-bar_left">
                <div class="content">
                    <span class="album-link">
                        <!-- Blank image before AJAX loads the album image. -->
                        <img class="album-artwork"
                             src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                             alt="Album image">
                    </span>

                    <div class="track-info text-white">
                        <span class="track-name">
                            <span></span>
                        </span>

                        <span class="artist-name">
                            <span></span>
                        </span>
                    </div>
                </div>
            </div>
            <!--endregion-->

            <!--region Playing Bar Center-->
            <div id="playing-bar_center">
                <div class="content player-controls">
                    <div class="buttons">
                        <button class="control-button shuffle" title="Aléatoire">
                            <img src="views/assets/images/icons/shuffle.png" alt="Aléatoire">
                        </button>

                        <button class="control-button previous" title="Précédant">
                            <img src="views/assets/images/icons/previous.png" alt="Précédant">
                        </button>

                        <button class="control-button play" title="Jouer">
                            <img src="views/assets/images/icons/play.png" alt="Jouer">
                        </button>

                        <button class="control-button pause" title="Pause" style="display: none">
                            <img src="views/assets/images/icons/pause.png" alt="Pause">
                        </button>

                        <button class="control-button next" title="Suivant">
                            <img src="views/assets/images/icons/next.png" alt="Suivant">
                        </button>

                        <button class="control-button repeat" title="Répéter">
                            <img src="views/assets/images/icons/repeat.png" alt="Répéter">
                        </button>
                    </div>

                    <div class="play-container">
                        <span class="play-time current my-auto">0.00</span>
                        <div class="play-time-bar my-auto">
                            <div class="background my-auto">
                                <div class="foreground my-auto"></div>
                            </div>
                        </div>
                        <span class="play-time remaining my-auto">0.00</span>
                    </div>
                </div>
            </div>
            <!--endregion-->

            <!--region Playing Bar Right-->
            <div id="playing-bar_right" class="text-right mt-4">
                <div class="volume-bar">
                    <button class="control-button volume" title="Volume">
                        <img src="views/assets/images/icons/volume.png" alt="Volume">
                    </button>

                    <div class="play-time-bar">
                        <div class="background">
                            <div class="foreground"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--endregion-->
        </div>
    </div>
</footer>
