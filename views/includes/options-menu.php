<?php
/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - options-menu.php
 * Description  :   [Description]
 *
 * Created      :   10.08.2019
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

use SelfTunes\Models\Database\Entity\Playlists;

?>

<nav class="options-menu">
    <input type="hidden" class="song-id">
    <select class="item add-song-playlist d-flex align-items-center">
        <option value="0">Ajouter à une playlist</option>
        <?php /** @var Playlists[] $userPlaylists */
        foreach ($userPlaylists as $userPlaylist): ?>
            <option value="<?= $userPlaylist->getIdPlaylist() ?>"><?= $userPlaylist->getName() ?></option>
        <?php endforeach ?>
    </select>
    <?php if ($_GET['action'] === 'playlist'): ?>
    <div class="item remove-song-playlist d-flex align-items-center" data-id="<?= $playlist->getIdPlaylist() ?>">
        Retirer de la playlist
    </div>
    <?php endif ?>
</nav>
