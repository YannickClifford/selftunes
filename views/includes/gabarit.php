<?php
/**
 * Author       :   Yannick.BAUDRAZ@cpnv.ch
 * Project      :   SelfTunes - gabarit.php
 * Description  :   Template page structure
 *
 * Created      :   07.07.2019
 * Updates      :   [dd.mm.yyyy author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

define('IS_LOGIN_ACTION', isset($_GET['action']) && $_GET['action'] == "login");
define('IS_REGISTER_ACTION', (isset($_GET['action']) && $_GET['action'] == "register"));
?>

<?php if (!IS_AJAX_REQ): ?>
    <!DOCTYPE html>
    <html lang="fr">

    <head>
        <?php include_once 'head.php'; ?>
        <title><?= $title ?></title>
        <?php include_once 'js-loader.php' ?>
    </head>

    <body>
        <div id="main-container" class="mt-3">
            <div id="top-container" class="container">
                <?php if (!IS_LOGIN_ACTION && !IS_REGISTER_ACTION) include_once 'nav-bar.php'; ?>
                <?php if (!IS_LOGIN_ACTION && !IS_REGISTER_ACTION): ?>
                    <div id="main-content" class="col-8 col-md-9 col-xl-10">
                        <?= $content ?>
                    </div>
                <?php else: ?>
                    <?= $content ?>
                <?php endif; ?>
            </div>
            <?php if (!IS_LOGIN_ACTION && !IS_REGISTER_ACTION) include_once 'playing-bar.php' ?>
        </div>
    </body>
    </html>
<?php else: ?>
    <?= $content ?>
<?php endif ?>
