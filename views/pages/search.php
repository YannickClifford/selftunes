<?php
/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - search.php
 * Description  :   [Description]
 *
 * Created      :   06.08.2019
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

use SelfTunes\Models\Database\Entity\{Albums, Artists, Songs};

ob_start();
$title = 'Recherche';
?>
    <div class="search-header">
        <div class="search-input-box">
            <input class="search-input display-1" placeholder="Commencez à taper..." spellcheck="false" type="text"
                   value="<?= $searchTerm ?>">
        </div>
    </div>

<?php
/**
 * @var Songs[]   $songs
 * @var Artists[] $artists
 * @var Albums[]  $albums
 */
if (!count($songs) && !count($artists) && !count($albums)): ?>
    <div class="search-content_empty">
        <h1 class="search-title">Rechercher dans SelfTunes</h1>
        <p class="search-subtitle">Retrouvez vos titres, artistes et albums préférés.</p>
    </div>
<?php else: ?>
    <div class="search-content container">
        <?php if (count($songs)): ?>
            <div class="track-list-container">
                <h2 class="display-3">Musiques</h2>
                <ul class="track-list">
                    <?php
                    foreach ($songs as $song): ?>
                        <li class="track-list-row">
                            <div class="track-count">
                                <img alt="Icône play" class="play" data-id="<?= $song->getIdSong() ?>"
                                     src="views/assets/images/icons/play-white.png">
                                <span class="track-number"><?= $song->row; ?></span>
                            </div>

                            <div class="track-info">
                                <span class="track-name"><?= $song->getTitle() ?></span>
                                <div class="d-flex">
                                    <a class="artist-href" data-title="<?= $song->artistName ?>"
                                       href="index.php?action=artist&id=<?= $song->getAlbumId() ?>">
                                        <span class="album-name"><?= $song->artistName ?></span>
                                    </a>
                                    &nbsp;-&nbsp;
                                    <a class="album-href" data-title="<?= $song->albumName ?>"
                                       href="index.php?action=album&id=<?= $song->getAlbumId() ?>">
                                        <span class="album-name"><?= $song->albumName ?></span>
                                    </a>
                                </div>
                            </div>

                            <div class="track-options">
                                <img alt="Options" class="options-button" data-id="<?= $song->getIdSong() ?>"
                                     src="views/assets/images/icons/more.png">
                            </div>

                            <div class="track-duration">
                                <span class="duration"><?= ltrim($song->getDuration(), 0) ?></span>
                            </div>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>

        <?php endif ?>

        <?php if ((count($songs) && count($artists))
            || (count($songs) && count($albums) && !count($artists))): ?>
            <hr>
        <?php endif ?>

        <?php if (count($artists)): ?>
            <h1 class="display-3">Artistes</h1>
            <div class="d-flex justify-content-around flex-wrap">
                <?php foreach ($artists as $artist): ?>
                    <div>
                        <div class="card bg-default">
                            <div class="card-body text-center">
                                <h4><?= $artist->getName() ?></h4>
                                <a class="artist-href btn btn-primary" data-title="<?= $artist->getName() ?>"
                                   href="index.php?action=artist&id=<?= $artist->getIdArtist() ?>">
                                    Page de l'artiste
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        <?php endif ?>

        <?php if (count($artists) && count($albums)): ?>
            <hr>
        <?php endif ?>

        <?php if (count($albums)): ?>
            <h1 class="display-3">Albums</h1>
            <div class="row">
                <?php foreach ($albums as $album): ?>
                    <div class="album-box col-sm-6 col-md-6 col-lg-6 col-xl-3 col-xxxl-2" style="width: 20rem;">
                        <a class="album-href" data-title="<?= $album->getName() ?>"
                           href="index.php?action=album&id=<?= $album->getIdAlbum() ?>">
                            <img src="<?= $album->getImage() ?>" alt="Album">
                            <div class="album-info text-center mb-5 mt-2">
                                <p class="mt-1 text-secondary"><?= $album->getName() ?></p>
                            </div>
                        </a>
                    </div>
                <?php endforeach ?>
            </div>
        <?php endif ?>
    </div>
<?php endif ?>


    <!--suppress JSJQueryEfficiency -->
    <script>
        if (typeof tempSongs === 'undefined')
            tempSongs = <?= json_encode($songs) ?>;

        if ($('#top-container').length) {
            resetSearchLayout = true
            if (typeof mainContentJQ === 'undefined') { var mainContentJQ = $('#main-content') }
            mainContentJQ.unwrap()
            mainContentJQ.css({
                'paddingRight': '0',
                'paddingLeft': '0'
            })
        }

        if (typeof caretPosition === 'undefined') caretPosition = null
        $('.search-input').on('focus', function () { $(this).caret(caretPosition) })
        $('.search-input').trigger('focus')
    </script>
<?php
include 'views/includes/options-menu.php';
$content = ob_get_clean();
require 'views/includes/gabarit.php';
