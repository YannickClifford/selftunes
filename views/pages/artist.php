<?php
/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - artist.php
 * Description  :   [Description]
 *
 * Created      :   30.07.2019
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

use SelfTunes\Models\Database\Entity\Albums;
use SelfTunes\Models\Database\Entity\Artists;
use SelfTunes\Models\Database\Entity\Songs;

/** @var Artists $artist */

ob_start();
$title = 'Artiste';
?>
    <div class="entity-info">
        <div class="center-section">
            <div class="artist-info text-center">
                <h1 class="artist-name display-1"><?= $artist->getName() ?></h1>
                <div class="header-buttons">
                    <button class="btn btn-success btn-round artist-play">Écouter toutes<br>les musiques</button>
                </div>
            </div>
        </div>
    </div>

    <hr>

    <div class="track-list-container">
        <h2 class="display-3">Populaires</h2>
        <ul class="track-list">
            <?php
            /** @var Songs $song */
            foreach ($artistSongs as $song): ?>
                <li class="track-list-row">
                    <div class="track-count">
                        <img alt="Icône play" class="play" data-id="<?= $song->getIdSong() ?>"
                             src="views/assets/images/icons/play-white.png">
                        <span class="track-number"><?= $song->row; ?></span>
                    </div>

                    <div class="track-info">
                        <span class="track-name"><?= $song->getTitle() ?></span>
                        <a class="album-href" data-title="<?= $song->albumName ?>"
                           href="index.php?action=album&id=<?= $song->getAlbumId() ?>">
                            <span class="album-name"><?= $song->albumName ?></span>
                        </a>
                    </div>

                    <div class="track-options">
                        <img alt="Options" class="options-button" data-id="<?= $song->getIdSong() ?>"
                             src="views/assets/images/icons/more.png">
                    </div>

                    <div class="track-duration">
                        <span class="duration"><?= ltrim($song->getDuration(), 0) ?></span>
                    </div>
                </li>
            <?php endforeach ?>
        </ul>
    </div>

    <hr>

    <h1 class="display-3">Albums</h1>
    <div class="row">
        <?php /** @var Albums $album */
        foreach ($artistAlbums as $album): ?>
            <div class="album-box col-sm-6 col-md-6 col-lg-6 col-xl-3 col-xxxl-2" style="width: 20rem;">
                <a class="album-href" data-title="<?= $album->getName() ?>"
                   href="index.php?action=album&id=<?= $album->getIdAlbum() ?>">
                    <img src="<?= $album->getImage() ?>" alt="Album">
                    <div class="album-info text-center mb-5 mt-2">
                        <p class="mt-1 text-secondary"><?= $album->getName() ?></p>
                    </div>
                </a>
            </div>
        <?php endforeach ?>
    </div>

    <script>
        tempSongs = <?= json_encode($artistSongs) ?>;
        tempArtistSongs = <?= json_encode($artistAllSongs) ?>;
    </script>
<?php
include 'views/includes/options-menu.php';
$content = ob_get_clean();
require 'views/includes/gabarit.php';
