<?php
/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - register.php
 * Description  :   [Description]
 *
 * Created      :   29.07.2019
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

ob_start();
$title = 'Inscription';
?>
    <div class="col d-flex align-items-center mx-auto" id="register-content"
         style="height: 100vh; max-width: 600px">
        <div class="card">
            <form action="index.php?action=register" class="form needs-validation" method="post" novalidate>
                <!--region Header-->
                <div class="card-header">
                    <img alt="Card image" class="card-img" src="views/assets/images/square-blue.png">
                    <h1><?= $title ?></h1>
                </div>
                <!--endregion-->

                <div class="alert alert-<?= !empty($_GET['registerError']) ? 'danger' : 'info'; ?> mx-3">
                    <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="tim-icons icon-simple-remove"></i>
                    </button>
                    <span>
                        <?php if (!empty($_GET['registerError'])): ?>
                            Erreur dans l'enregistrement, pour rappel l'email
                            doit être disponible sur le site, les
                        <?php endif ?>
                        3 premiers champs et la case à cocher sont obligatoires et les mots de passes doivent être identiques.</span>
                </div>

                <!--region Inputs-->
                <div class="card-body">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="tim-icons icon-email-85"></i></span>
                        </div>
                        <input autocomplete="email" class="form-control" name="email" placeholder="Email"
                               type="email" required>
                        <div class="invalid-feedback">Obligatoire et doit être une adresse email</div>
                    </div>

                    <div class="form-row">
                        <div class="input-group col-6">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="tim-icons icon-lock-circle"></i></span>
                            </div>
                            <input autocomplete="new-password" class="form-control" name="password"
                                   placeholder="Mot de passe" type="password" required>
                            <div class="invalid-feedback">Obligatoire</div>
                        </div>

                        <div class="input-group col-6">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="tim-icons icon-lock-circle"></i></span>
                            </div>
                            <input autocomplete="new-password" class="form-control" name="repeat-password"
                                   placeholder="Retaper le mot de passe" type="password" required>
                            <div class="invalid-feedback">Obligatoire</div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="input-group col-6">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="tim-icons icon-single-02"></i></span>
                            </div>
                            <input autocomplete="fname" class="form-control" name="fname" placeholder="Prénom"
                                   type="text">
                        </div>

                        <div class="input-group col-6">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="tim-icons icon-single-02"></i></span>
                            </div>
                            <input autocomplete="lname" class="form-control" name="lname"
                                   placeholder="Nom de famille" type="text">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" name="check" required>
                                J'ai inscrit les bonnes informations
                                <span class="form-check-sign"><span class="check"></span></span>
                                <span class="invalid-feedback">Obligatoire</span>
                            </label>

                        </div>
                    </div>
                </div>
                <!--endregion-->

                <!--region Footer-->
                <div class="card-footer text-center mt-0">
                    <input class="btn btn-info btn-round btn-lg btn-block" type="submit" value="S'inscrire"
                           name="register">
                </div>
                <div class="pull-left ml-3 mb-3">
                    <h6>
                        <a class="link text-info footer-link" href="index.php?action=login">Se connecter</a>
                    </h6>
                </div>
                <!--endregion-->
            </form>
        </div>
    </div>
<?php
$content = ob_get_clean();
require 'views/includes/gabarit.php';

