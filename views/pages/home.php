<?php
/**
 * Author       :   Yannick.BAUDRAZ@cpnv.ch
 * Project      :   SelfTunes - home.php
 * Description  :   Home page
 *
 * Created      :   07.07.2019
 * Updates      :   [dd.mm.yyyy author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

use SelfTunes\Models\Database\Entity\Albums;

ob_start();
$title = 'Accueil';
?>
    <h1 class="display-1 p-4 text-center">Vous Pourriez Aimer</h1>
    <div class="row">
        <?php /** @var Albums $album */
        foreach ($albums as $album): ?>
            <div class="album-box col-sm-6 col-md-6 col-lg-6 col-xl-3 col-xxxl-2">
                <a class="album-href" data-title="<?= $album->getName() ?>"
                   href="index.php?action=album&id=<?= $album->getIdAlbum() ?>">
                    <img src="<?= $album->getImage() ?>" alt="Album">
                </a>
                <div class="album-info text-center mb-5 mt-2">
                    <p class="mt-1 text-secondary album-href" data-title="<?= $album->getName() ?>"
                       href="index.php?action=album&id=<?= $album->getIdAlbum() ?>"><?= $album->getName() ?></p>
                    <p class="artist-href" role="link" data-title="<?= $album->artistName ?>"
                       href="index.php?action=artist&id=<?= $album->getArtistId() ?>"><?= $album->artistName ?></p>
                </div>
            </div>
        <?php endforeach ?>
    </div>
<?php
$content = ob_get_clean();
require 'views/includes/gabarit.php';
