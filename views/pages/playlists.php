<?php
/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - playlist.php
 * Description  :   [Description]
 *
 * Created      :   07.08.2019
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

use SelfTunes\Models\Database\Entity\Playlists;

ob_start();
$title = 'Playlists'
?>
    <div class="playlists-container">
        <h1 class="display-1 p-4 text-center">Vos playlists</h1>
        <div class="button-items">
            <button class="btn btn-success btn-round">Nouvelle playlist</button>
        </div>
        <div class="d-flex flex-wrap mt-5">
            <?php /** @var Playlists[] $userPlaylists */
            foreach ($userPlaylists as $playlist): ?>
                <div class="mr-3 col-sm-6 col-md-6 col-lg-6 col-xl-3 col-xxxl-2">
                    <a class="playlist-href" data-title="<?= $playlist->getName() ?>"
                       href="index.php?action=playlist&id=<?= $playlist->getIdPlaylist() ?>">
                        <img class="playlist-img" src="views/assets/images/icons/playlist.png" alt="Playlist">
                        <div class="text-center mb-5 mt-2">
                            <p class="font-weight-bold text-primary"><?= $playlist->getName() ?></p>
                        </div>
                    </a>
                </div>
            <?php endforeach ?>
        </div>
    </div>
<?php
$content = ob_get_clean();
require 'views/includes/gabarit.php';
