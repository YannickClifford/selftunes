<?php
/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - account.php
 * Description  :   [Description]
 *
 * Created      :   12.08.2019
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

use SelfTunes\Models\Database\Entity\Users;

/** @var Users $user */

ob_start();
$title = 'Compte de ' . $user->getFirstname() . ' ' . $user->getLastname();
?>
    <div class="row">
        <div class="col-md-5">
            <div class="section">
                <!-- User Information -->
                <section class="text-center">
                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                        <div class="fileinput-new thumbnail img-circle">
                            <img src="views/assets/images/accounts/selftunes_account_generic.png"
                                 alt="...">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail img-circle img-raised"></div>
                        <div>
                        <span class="btn btn-raised btn-round btn-neutral btn-file">
                          <span class="fileinput-new">Ajouter une photo</span>
                          <span class="fileinput-exists">Changer</span>
                          <input type="file" name="...">
                        </span>
                            <br>
                            <a href="#" class="btn btn-danger btn-round fileinput-exists btn-simple"
                               data-dismiss="fileinput"><i class="tim-icons icon-simple-remove"></i> Retirer</a>
                        </div>
                    </div>
                    <?php if (!empty($user->getFirstname()) || !empty($user->getLastname())): ?>
                        <h3 class="title"><?= $user->getFirstname() ?> <?= $user->getLastname() ?></h3>
                    <?php endif ?>
                </section>
                <!-- User Information -->
            </div>
        </div>
        <div class="col-md-7 ml-auto">
            <div class="section">
                <div class="tab-content">
                    <header>
                        <h2 class="text-uppercase">Informations du compte</h2>
                    </header>
                    <hr class="line-primary">
                    <br>
                    <?php if (!empty($_GET['error-update'])): ?>
                        <?php if ($_GET['error-update'] === 'true'): ?>
                            <div class="alert alert-danger">
                                <button aria-hidden="true" aria-label="Close" class="close" data-dismiss="alert"
                                        type="button">
                                    <i class="tim-icons icon-simple-remove"></i>
                                </button>
                                <span>Une erreur s'est produite, veuillez remplir les informations avec précautions.</span>
                            </div>
                        <?php else: ?>
                            <div class="alert alert-success">
                                <button aria-hidden="true" aria-label="Close" class="close" data-dismiss="alert"
                                        type="button">
                                    <i class="tim-icons icon-simple-remove"></i>
                                </button>
                                <span>Votre compte a été correctement modifié.</span>
                            </div>
                        <?php endif ?>
                    <?php endif ?>
                    <form action="index.php?AJAX=update-account" id="account-info" method="post">
                        <div class="row">
                            <div class="col-md-3 align-self-center">
                                <label class="labels" for="fname">Prénom</label>
                            </div>
                            <div class="col-md-9 align-self-center">
                                <div class="form-group">
                                    <input id="fname" name="fname" class="form-control" type="text"
                                           value="<?= $user->getFirstname() ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 align-self-center">
                                <label class="labels" for="lname">Nom de famille</label>
                            </div>
                            <div class="col-md-9 align-self-center">
                                <div class="form-group">
                                    <input id="lname" name="lname" class="form-control" type="text"
                                           value="<?= $user->getLastname() ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 align-self-center">
                                <label class="labels" for="email">Email</label>
                            </div>
                            <div class="col-md-9 align-self-center">
                                <div class="form-group">
                                    <input autocomplete="username email" class="form-control" id="email" name="email"
                                           required type="email" value="<?= $user->getEmail() ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 align-self-center">
                                <label class="labels" for="repeat-email">Confirmer Email</label>
                            </div>
                            <div class="col-md-9 align-self-center">
                                <div class="form-group">
                                    <input class="form-control" id="repeat-email" name="repeat-email" required
                                           type="email" value="<?= $user->getEmail() ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 align-self-center">
                                <label class="labels" for="password" data-toggle="tooltip"
                                       title="Si ce champs n'est pas vide, les deux mots de passe doivent correspondre
                                       pour que le mot de passe soit changé.">
                                    Mot de passe
                                </label>
                            </div>
                            <div class="col-md-9 align-self-center">
                                <div class="form-group">
                                    <input autocomplete="new-password" class="form-control" id="password"
                                           name="password" type="password" value="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 align-self-center">
                                <label class="labels" for="repeat-password">Confirmer mot de passe</label>
                            </div>
                            <div class="col-md-9 align-self-center">
                                <div class="form-group">
                                    <input autocomplete="new-password" class="form-control" id="repeat-password"
                                           name="repeat-password" type="password" value="">
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col">
                                <input type="submit" class="btn btn-primary" value="Enregistrer">
                                <input type="reset" class="btn btn-primary btn-simple" value="Annuler">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php
$content = ob_get_clean();
require 'views/includes/gabarit.php';
