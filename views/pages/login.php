<?php
/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - login.php
 * Description  :   [Description]
 *
 * Created      :   28.07.2019
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

ob_start();
$title = 'Connexion';
?>
    <div class="col d-flex align-items-center mx-auto" id="login-content"
         style="height: 100vh; max-width: 600px">
        <div class="card">
            <form action="index.php?action=login" class="form needs-validation" method="post" novalidate>
                <!--region Header-->
                <div class="card-header">
                    <img alt="Card image" class="card-img" src="views/assets/images/square-purple.png">
                    <h1><?= $title ?></h1>
                </div>
                <!--endregion-->

                <?php if (!empty($_GET['loginError'])): ?>
                    <div class="alert alert-danger mx-3">
                        <button aria-hidden="true" aria-label="Close" class="close" data-dismiss="alert" type="button">
                            <i class="tim-icons icon-simple-remove"></i>
                        </button>
                        <span>Identification incorrecte</span>
                    </div>
                <?php endif ?>

                <!--region Inputs-->
                <div class="card-body">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="tim-icons icon-email-85"></i></span>
                        </div>
                        <input autocomplete="email" class="form-control" name="email" placeholder="Email" required
                               type="email">
                        <div class="invalid-feedback">Obligatoire</div>
                    </div>

                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="tim-icons icon-lock-circle"></i></span>
                        </div>
                        <input autocomplete="new-password" class="form-control" name="password"
                               placeholder="Mot de passe" required type="password">
                        <div class="invalid-feedback">Obligatoire</div>
                    </div>
                </div>
                <!--endregion-->

                <!--region Footer-->
                <div class="card-footer text-center">
                    <input class="btn btn-primary btn-round btn-lg btn-block" type="submit" value="Se connecter">
                </div>
                <div class="pull-left ml-3 mb-3">
                    <h6>
                        <a class="link footer-link" href="index.php?action=register">Créer une compte</a>
                    </h6>
                </div>
                <!--endregion-->
            </form>
        </div>
    </div>
<?php
$content = ob_get_clean();
require 'views/includes/gabarit.php';
