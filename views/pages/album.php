<?php
/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - album.php
 * Description  :   [Description]
 *
 * Created      :   23.07.2019
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

use SelfTunes\Models\Database\Entity\Albums;
use SelfTunes\Models\Database\Entity\Songs;

/** @var Albums $album */
/** @var Songs $track */

ob_start();
$title = 'Albums';
?>
    <div class="single-album-info">
        <div class="left-section">
            <img class="img-fluid shadow-lg" src="<?= $album->getImage() ?>" alt="Album image">
        </div>

        <div class="right-section">
            <h2><?= $album->getName() ?></h2>
            <p>By
                <a class="artist-href" data-title="<?= $album->artistName ?>"
                   href="index.php?action=artist&id=<?= $album->getArtistId() ?>">
                    <?= $album->artistName ?>
                </a>.
            </p>
            <p class="track-numbers">
                <?= $album->getYear() . ' - ' . $album->tracksNumber ?> Titres - <?= $album->genre ?>
            </p>
        </div>
    </div>

    <div class="track-list-container">
        <ul class="track-list">
            <?php
            foreach ($albumTracks as $track): ?>
                <li class="track-list-row">
                    <div class="track-count">
                        <img alt="Icône play" class="play" data-id="<?= $track->getIdSong() ?>"
                             src="views/assets/images/icons/play-white.png">
                        <span class="track-number"><?= $track->getTrackNumber(); ?></span>
                    </div>

                    <div class="track-info">
                        <span class="track-name"><?= $track->getTitle() ?></span>
                        <a href="index.php?action=artist&id=<?= $album->getArtistId() ?>"
                           data-title="<?= $album->artistName ?>"
                           class="artist-name artist-href"><?= $album->artistName ?></a>
                    </div>

                    <div class="track-options">
                        <img alt="Options" class="options-button" data-id="<?= $track->getIdSong() ?>"
                             src="views/assets/images/icons/more.png">
                    </div>

                    <div class="track-duration">
                        <span class="duration"><?= ltrim($track->getDuration(), 0) ?></span>
                    </div>
                </li>
            <?php endforeach ?>
        </ul>
    </div>

    <script>
        tempSongs = <?= json_encode($albumTracks) ?>;
    </script>
<?php
include 'views/includes/options-menu.php';
$content = ob_get_clean();
require 'views/includes/gabarit.php';
