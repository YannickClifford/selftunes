<?php
/**
 * Author       :   Yannick Baudraz - <Yannick.BAUDRAZ@cpnv.ch>
 * Project      :   SelfTunes - playlist.php
 * Description  :   [Description]
 *
 * Created      :   08.08.2019
 * Updates      :   [dd.mm.yyyy - author]
 *                      [description of update]
 *
 * Git source   :   [git source]
 *
 * Created with PhpStorm.
 */

use SelfTunes\Models\Database\Entity\Playlists;
use SelfTunes\Models\Database\Entity\Songs;

/* @var Playlists $playlist */

ob_start();
$title = 'Playlists';
?>
    <div class="single-album-info">
        <div class="left-section">
            <img class="playlist-img" src="views/assets/images/icons/playlist.png" alt="Playlist image">
        </div>

        <div class="right-section">
            <h2 class="text-primary"><?= $playlist->getName() ?></h2>
            <p class="track-numbers">
                <?= $playlist->tracksNumber ?> Titres
                <span class="text-muted">
                    . Créée le <?= changeDateFormat($playlist->getDateCreation(), '%e %B %Y à %H:%M:%S') ?>
                </span>
            </p>
            <button class="btn btn-sm btn-warning" data-target="#delPlaylistModal" data-toggle="modal">
                Supprimer la playlist
            </button>

            <!-- Modal -->
            <div class="modal fade" id="delPlaylistModal" tabindex="-1" role="dialog"
                 aria-labelledby="delPlaylistModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="delPlaylistModalLabel">Suppression de votre playlist</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Êtes-vous certains de vouloir supprimer cette playlist ?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Non</button>
                            <button type="button" class="delete-playlist btn btn-danger"
                                    data-id="<?= $playlist->getIdPlaylist() ?>">Oui
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="track-list-container">
        <ul class="track-list">
            <?php
            /** @var Songs[] $playlistTracks */
            foreach ($playlistTracks as $track): ?>
                <li class="track-list-row">
                    <div class="track-count">
                        <img alt="Icône play" class="play" data-id="<?= $track->getIdSong() ?>"
                             src="views/assets/images/icons/play-white.png">
                        <img src="views/assets/images/icons/track.png" alt="Icône musique">
                    </div>

                    <div class="track-info">
                        <span class="track-name"><?= $track->getTitle() ?></span>
                        <a href="index.php?action=artist&id=<?= $track->idArtist ?>"
                           data-title="<?= $track->artistName ?>"
                           class="artist-name artist-href"><?= $track->artistName ?></a>
                    </div>

                    <div class="track-options">
                        <img alt="Options" class="options-button" data-id="<?= $track->getIdSong() ?>"
                             src="views/assets/images/icons/more.png">
                    </div>

                    <div class="track-duration">
                        <span class="duration"><?= ltrim($track->getDuration(), 0) ?></span>
                    </div>
                </li>
            <?php endforeach ?>
        </ul>
    </div>

    <script>
        tempSongs = <?= json_encode($playlistTracks) ?>;
    </script>
<?php
include 'views/includes/options-menu.php';
$content = ob_get_clean();
require 'views/includes/gabarit.php';
