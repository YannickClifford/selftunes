import {openPage} from './pages-ajax.js'

const mainContentJQ = $('#main-content')

/**
 * This function hide the song options menu.
 */
function hideOptionsMenu() {
    const menuEltJQ = $('.options-menu')
    if (menuEltJQ.css('display') !== 'none')
        menuEltJQ.css('display', 'none')
}

/**
 * Create a playlist.
 */
mainContentJQ.on('click', '.playlists-container .button-items button', function () {
    let inputFromUser = prompt('Entrez le nom de votre playlist')

    if (inputFromUser) {
        $.post('index.php?AJAX=create-playlist', {playlistName: inputFromUser})
         .done(function () {
             openPage('index.php?action=playlists')
         })
    }
})

/**
 * Delete playlist.
 */
mainContentJQ.on('click', '.delete-playlist', function () {
    // noinspection JSUnresolvedFunction
    $('#delPlaylistModal').modal('hide')
    const playlistId = this.getAttribute('data-id')
    $.post('index.php?AJAX=delete-playlist', {playlistId: playlistId})
     .done(function () {
         openPage('index.php?action=playlists')
     })
})

/**
 * Show song options menu when clicking on img.
 */
mainContentJQ.on('click', '.options-button', function (event) {
    const menuEltJQ = $('.options-menu')
    const top = event.clientY + 'px'
    const left = event.clientX + 1 + 'px'

    menuEltJQ.find('.song-id').val(this.getAttribute('data-id'))
    menuEltJQ.css({
        'top': top,
        'left': left,
        'display': 'inline'
    })
})

/**
 * Hide song options menu when scrolling the page.
 */
$(window).on('scroll', function () {
    hideOptionsMenu()
})

/**
 * Hide song options menu when clicking one the document.
 */
$(document).on('click', function (event) {
    if (!$(event.target).hasClass('item') && !$(event.target).hasClass('options-button'))
        hideOptionsMenu()
})

/**
 * Add song in a playlist.
 */
mainContentJQ.on('change', '.options-menu .add-song-playlist', function () {
    const playlistId = $(this).val()
    const songId = $(this).prev('.song-id').val()

    hideOptionsMenu()
    $(this).val('0')

    $.post('index.php?AJAX=add-to-playlist', {playlistId: playlistId, songId: songId})
})

/**
 * Remove song from a playlist.
 */
mainContentJQ.on('click', '.options-menu .remove-song-playlist', function () {
    const playlistId = this.getAttribute('data-id')
    const songId = $(this).prevAll('.song-id').val()

    $.post('index.php?AJAX=remove-from-playlist', {playlistId: playlistId, songId: songId})
     .done(function () {
        openPage(window.location.href)
     })
})
