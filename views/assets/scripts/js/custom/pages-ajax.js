import {setTrack, arrayOfOneField} from './music-player.js'

const mainContentJQ = $('#main-content')
let timer

/**
 * Open the page with an AJAX request (without reload the page).
 *
 * @param {String} url
 * @param {Event} event
 * @param {String} title
 */
export function openPage(url, event = null, title = null) {
    if (timer)
        clearTimeout(timer)

    mainContentJQ.load(encodeURI(url), function () {
        $(document).scrollTop(0)
        history.pushState(null, null, url)
        if (title !== null) document.title = title
    })

    if (event)
        event.preventDefault()
}

$('.logo').on('click', function (event) {
    $('.nav-link[data-toggle="tab"]').removeClass('active show')
    $('#nav-pills-container .nav-item:first-child a').addClass('active show')
    openPage('index.php', event, 'Accueil')
})

$('#search-btn-container .nav-link[data-toggle="tab"]').on('click', function (event) {
    $('#nav-pills-container a.active').removeClass('active show')
    openPage('index.php?action=search', event, 'Recherche')
})

$('#nav-pills-container .nav-link[data-toggle="tab"]').on('click', function (event) {
    $('#search-btn-container a.active').removeClass('active show')
    openPage($(this).attr('href'), event, $(this).text())
})

mainContentJQ.on('click', '.album-href, .artist-href, .playlist-href', function (event) {
    $('#nav-bar a.active').removeClass('active show')
    openPage(this.getAttribute('href'), event, this.getAttribute('data-title'))
})

mainContentJQ.on('input', '.search-header', function (event) {
    clearTimeout(timer)
    timer = setTimeout(function () {
        const searchInputJQ = $('.search-input')
        window.caretPosition = searchInputJQ.caret()
        const value = searchInputJQ.val()
        openPage('index.php?action=search&search-term=' + value, event)
    }, 500)
})

mainContentJQ.on('submit', '#account-info', function (event) {
    event.preventDefault()
    $.post({url: this.action, data: $(this).serialize()}).done(function (error) {
        openPage('index.php?action=account&error-update=' + error)
    })
})

$(document).ajaxComplete(function () {
    const reloadPlayTrackEvent = function () {
        $('.track-count .play').on('click', function () {
            const tempSongsId = arrayOfOneField(tempSongs, 'id_song')
            setTrack(Number(this.getAttribute('data-id')), tempSongsId)
        })
    }
    const reloadPlayArtistEvent = function () {
        let tempArtistSongsId = arrayOfOneField(tempArtistSongs, 'id_song')
        $('.artist-play').on('click', function () {
            setTrack(tempArtistSongsId[0], tempArtistSongsId)
        })
    }

    const url = arguments[2].url
    switch (true) {
        case /action=artist&/.test(url):
            reloadPlayTrackEvent()
            reloadPlayArtistEvent()
            break
        case /action=album&/.test(url):
        case /action=search/.test(url):
        case /action=playlist&/.test(url):
            reloadPlayTrackEvent()
            break
    }

    if (!/action=search/.test(url) && !/AJAX=get-song-json/.test(url) && !/AJAX=update-plays/.test(url))
        if (!$('#top-container').length)
            $('#nav-bar, #main-content')
                .wrapAll('<div id="top-container" class="container"></div>')
})
