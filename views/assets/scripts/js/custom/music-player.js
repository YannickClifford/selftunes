//region Initialization
import {Audio} from './Audio.js'

/**
 * Audio object
 * @type {Audio}
 */
const audio = new Audio()

//region Playlist variables
/**
 * Array of id from the object passed by PHP.
 * @type {Array}
 */
let currentPlaylist = arrayOfOneField(arrayOfSongs, 'id_song')
const tempSongsId = typeof tempSongs !== 'undefined' ? arrayOfOneField(tempSongs, 'id_song') : null
let tempArtistSongsId = typeof tempArtistSongs !== 'undefined' ? arrayOfOneField(tempArtistSongs, 'id_song') : null
/**
 * To shuffle currentPlaylist.
 *
 * @type {Array}
 */
let shufflePlaylist = []
/**
 * Current index of the song in the array,
 * relative to arrayOfId.
 *
 * @type {number}
 */
let currentIndex = 0
//endregion

//region HTML elements
const playBtnJQ = $('.control-button.play')
const pauseBtnJQ = $('.control-button.pause')
const previousBtnJQ = $($('.control-button.previous'))
const nextBtnJQ = $('.control-button.next')
const shuffleBtnJQ = $('.control-button.shuffle img')
const repeatBtnJQ = $('.control-button.repeat img')
const volumeBtnJQ = $('.control-button.volume img')
const playAlbumTrackBtnJQ = $('.track-count .play')
const playArtistBtnJQ = $('.artist-play')
const playBarJQ = $('.play-container .play-time-bar')
const playBarFgJQ = $('.play-container .play-time-bar .foreground')
const volumeBarJQ = $('.volume-bar .play-time-bar')
//endregion

//region Flags
export let mouseDownPlayer = false
export let mouseDownVolume = false
//endregion
//endregion

setTrack(currentPlaylist[0], currentPlaylist, false)

//region Functions
/**
 * Set the track to play and play it if needed.
 *
 * @param {number} trackId
 * @param {[]} newPlaylist
 * @param {boolean} play
 */
export function setTrack(trackId, newPlaylist, play = true) {
    if (newPlaylist != currentPlaylist) {
        currentPlaylist = newPlaylist
        shufflePlaylist = currentPlaylist.slice()
        shuffleArray(shufflePlaylist)
    }

    if (audio.shuffle)
        currentIndex = shufflePlaylist.indexOf(trackId)
    else
        currentIndex = currentPlaylist.indexOf(trackId)

    $.post("index.php?AJAX=get-song-json", {songId: trackId}).done(function (data) {
        /**
         * Song for the music player
         *
         * @typedef {Object} Track
         * @var {Track} track
         * @property {number} id_song
         * @property {number} album_id
         * @property {number} artistName
         * @property {number} genre_id
         * @property {number} title
         * @property {number} duration
         * @property {number} path
         * @property {number} track_number
         * @property {number} plays
         * @property {number} albumImage
         */
        const track = JSON.parse(data)

        $('.track-name span').text(track.title)
        $('.artist-name span').text(track.artistName)
        $('.album-link img').attr("src", track.albumImage)
        playBarFgJQ.width(0)

        audio.loadTrack(track)

        if (play)
            playSong()
    })
}

/**
 * Play the song in the loader
 */
function playSong() {
    playBtnJQ.hide()
    pauseBtnJQ.show()

    // Update number of time the song is played, only if it's the beginning of the song.
    if (audio.audioElt.currentTime === 0)
        $.post("index.php?AJAX=update-plays", {songId: audio.currentlyPlayingSong.id_song})
    audio.play()
}

/**
 * Pause the song in the loader
 */
function pauseSong() {
    pauseBtnJQ.hide()
    playBtnJQ.show()
    audio.pause()
}

/**
 * Get the previous song and play it.
 * If seconds are more than 5 or it's the first song of the playlist, reload from the beginning of thh song.
 */
function previousSong(play = false) {
    if (audio.audioElt.currentTime >= 5 || currentIndex === 0 || audio.repeat)
        audio.setTime(0)
    else {
        currentIndex--
        setTrack(currentPlaylist[currentIndex], currentPlaylist, play ? play : !audio.audioElt.paused)
    }
}

/**
 * Get the next song and play it.
 * Replay the current song to beginning if repeat.
 */
export function nextSong() {
    if (audio.repeat === true) {
        audio.setTime(0)
        playSong()
    } else {
        if (currentIndex == currentPlaylist.length - 1)
            currentIndex = 0
        else
            currentIndex++

        const trackToPlay = audio.shuffle ? shufflePlaylist[currentIndex] : currentPlaylist[currentIndex]
        const play = (audio.audioElt.currentTime == audio.audioElt.duration) ? true : !audio.audioElt.paused
        setTrack(trackToPlay, currentPlaylist, play)
    }
}

/**
 * Set shuffle if not set.
 * If set, unset.
 */
function setShuffle() {
    audio.shuffle = !audio.shuffle
    const imageName = audio.shuffle ? 'shuffle-active.png' : 'shuffle.png'
    shuffleBtnJQ.attr('src', 'views/assets/images/icons/' + imageName)

    if (audio.shuffle) {
        if (shufflePlaylist.length === 0) shufflePlaylist = currentPlaylist.slice()
        shuffleArray(shufflePlaylist)
        currentIndex = shufflePlaylist.indexOf(audio.currentlyPlayingSong.id_song)
    } else {
        currentIndex = currentPlaylist.indexOf(audio.currentlyPlayingSong.id_song)
    }
}

/**
 * Set repeat if not set.
 * If set, unset.
 */
function setRepeat() {
    audio.repeat = !audio.repeat
    const imageName = audio.repeat ? 'repeat-active.png' : 'repeat.png'
    repeatBtnJQ.attr('src', 'views/assets/images/icons/' + imageName)
}

/**
 * Set mute if not set.
 * If set, unset.
 */
function setMute() {
    audio.audioElt.muted = !audio.audioElt.muted
    const imageName = audio.audioElt.muted ? 'volume-mute.png' : 'volume.png'
    volumeBtnJQ.attr('src', 'views/assets/images/icons/' + imageName)
}

/**
 * Transform an object/array of object into an array with the field wanted.
 *
 * @param {Object|Array} list
 * @param {String} fieldName
 * @returns {Array}
 */
export function arrayOfOneField(list, fieldName) {
    const newArray = []

    $.each(list, function (index, column) {
        const keys = Object.keys(column)
        keys.forEach(function (key) {
            if (key === fieldName)
                newArray.push(column[key])
        })
    })

    return newArray
}

/**
 * Shuffles array in place.
 * It will shuffle the original array.
 *
 * @param {Array} array An array containing the items.
 */
function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]]
    }
}
//endregion

//region Events
//region Buttons
/**
 * Click on play button.
 */
playBtnJQ.on('click', function () {
    playSong()
})

/**
 * Click on pause button.
 */
pauseBtnJQ.on('click', function () {
    pauseSong()
})

/**
 * Click on previous button.
 */
previousBtnJQ.on('click', function () {
    previousSong()
})

/**
 * Click on next button.
 */
nextBtnJQ.on('click', function () {
    nextSong()
})

/**
 * Click on shuffle button.
 */
shuffleBtnJQ.on('click', function () {
    setShuffle()
})

/**
 * Click on repeat button.
 */
repeatBtnJQ.on('click', function () {
    setRepeat()
})

/**
 * Click on volume button.
 */
volumeBtnJQ.on('click', function () {
    setMute()
})

playAlbumTrackBtnJQ.on('click', function () {
    setTrack(Number(this.getAttribute('data-id')), tempSongsId)
})

playArtistBtnJQ.on('click', function () {
    setTrack(tempArtistSongsId[0], tempArtistSongsId)
})
//endregion

//region Playing bar
/**
 * Pressure on progress-bar music player.
 */
playBarJQ.on('mousedown', function () {
    mouseDownPlayer = true
})

/**
 * Movement on progress-bar music player.
 */
playBarJQ.on('mousemove', function (e) {
    if (mouseDownPlayer) {
        const progress = (e.offsetX * 100) / $(this).width()
        playBarFgJQ.css('width', progress + '%')
        $('body').css('user-select', 'none')
    }
})
//endregion

//region Volume bar
/**
 * Pressure on volume-bar.
 */
volumeBarJQ.on('mousedown', function () {
    mouseDownVolume = true
})

/**
 * Movement on volume-bar.
 */
volumeBarJQ.on('mousemove', function (e) {
    if (mouseDownVolume) {
        let percentage = e.offsetX / $(this).width()
        percentage = (percentage < 0.023) ? 0 : (percentage > 1) ? 1 : percentage
        audio.audioElt.volume = percentage
        $('body').css('user-select', 'none')
    }
})

/**
 * Release one the volume bar
 */
volumeBarJQ.on('mouseup', function (e) {
    audio.audioElt.volume = e.offsetX / $(this).width()
    mouseDownVolume = false
})
//endregion

//region HTML
/**
 * Release on the body
 */
$('html').on('mouseup', function (e) {
    if (mouseDownPlayer) {
        const rect = document.querySelector('.play-container .foreground').getBoundingClientRect(),
            offsetX = e.clientX - rect.left,
            seconds = audio.audioElt.duration * (offsetX / playBarJQ.width())

        audio.setTime(seconds)
        $('body').css('user-select', 'auto')
    } else if (mouseDownVolume) {
        const rect = document.querySelector('.volume-bar .foreground').getBoundingClientRect(),
            offsetX = e.clientX - rect.left
        if (offsetX < 0.023)
            audio.audioElt.volume = 0
        $('body').css('user-select', 'auto')
    }

    mouseDownPlayer = false
    mouseDownVolume = false
})
//endregion
//endregion
