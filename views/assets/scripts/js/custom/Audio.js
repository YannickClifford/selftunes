import {nextSong, mouseDownPlayer} from './music-player.js'

export class Audio {

    SERVER_URL = 'http://baudraz.internet-box.ch:1993/'

    /**
     * Audio HTML element
     *
     * @type {HTMLAudioElement}
     */
    audioElt = document.createElement('audio')

    /**
     * Current song in the music player
     *
     * @type {Track}
     */
    currentlyPlayingSong

    /**
     * Say if shuffle is set or not.
     *
     * @type {boolean}
     */
    shuffle = false

    /**
     * Say if repeat is set or not.
     *
     * @type {boolean}
     */
    repeat = false

    constructor() {
        this.audioElt.addEventListener('canplay', function () {
            $('.play-time.remaining').text(moment.unix(this.duration).format('m:ss'))
            $('.play-time.current').text(moment.unix(this.currentTime).format('m:ss'))
        })

        this.audioElt.addEventListener('ended', function () {
            nextSong(true)
        })

        this.audioElt.addEventListener('timeupdate', function () {
            if (this.duration) {
                $('.play-time.current').text(moment.unix(this.currentTime).format('m:ss'))
                if (!mouseDownPlayer) {
                    const progress = (this.currentTime * 100) / this.duration
                    $('.play-container .foreground').css('width', progress + '%')
                }
            }
        })

        this.audioElt.addEventListener('volumechange', function () {
            const volume = this.volume * 100
            $('.volume-bar .foreground').css('width', volume + '%')
        })
    }

    /**
     * Load the track
     *
     * @param {Track} track
     */
    loadTrack(track) {
        this.currentlyPlayingSong = track
        this.audioElt.src = this.SERVER_URL + track.path
    }

    /**
     * Play current song.
     *
     * @returns {Promise<void>}
     */
    play() {
        return this.audioElt.play()
    }

    /**
     * Pause the current song.
     */
    pause() {
        this.audioElt.pause()
    }

    /**
     * Set the current time of the current song.
     * @param seconds
     */
    setTime(seconds) {
        this.audioElt.currentTime = seconds
    }
}
